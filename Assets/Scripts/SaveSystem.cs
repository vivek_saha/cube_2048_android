using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class SaveSystem : MonoBehaviour
{


    void Start()
    {
       
    }

    public void SaveData()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/Data.dat");
        Debug.Log(Application.persistentDataPath);
        List<PlayerData> datas = new List<PlayerData>();

        foreach (GameObject cube in Controller_script.instance.List_of_cubes)
        {
            PlayerData dt = new PlayerData();
            dt._position[0] = cube.transform.localPosition.x;
            dt._position[1] = cube.transform.localPosition.y;
            dt._position[2] = cube.transform.localPosition.z;

            dt._rotation[0] = cube.transform.localRotation.w;
            dt._rotation[1] = cube.transform.localRotation.x;
            dt._rotation[2] = cube.transform.localRotation.y;
            dt._rotation[3] = cube.transform.localRotation.z;

            dt._scale[0] = cube.transform.localScale.x;
            dt._scale[1] = cube.transform.localScale.y;
            dt._scale[2] = cube.transform.localScale.z;

            dt.cube_type = cube.GetComponent<cube_script>().cube_typpe;
            datas.Add(dt);
        }
        bf.Serialize(file, datas);
        file.Close();
    }

    public void LoadData()
    {
        if (File.Exists(Application.persistentDataPath + "/Data.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/Data.dat", FileMode.Open);
            Debug.Log(Application.persistentDataPath);
            List<PlayerData> data = new List<PlayerData>();
            data = (List<PlayerData>)bf.Deserialize(file);
            file.Close();

            Controller_script.instance.Load_old_cubes(data);
        }
    }
}

public static class SaveSystem_s
{
    public static void Savegame()
    {
       SaveSystem s = new SaveSystem();
        s.SaveData();
    }
    public static void Loadgame()
    {
        SaveSystem s = new SaveSystem();
        s.LoadData();
    }
}


[Serializable]
public class PlayerData
{
    public string cube_type;
    public float[] _position = new float[3];
    public float[] _rotation = new float[4];
    public float[] _scale = new float[3];
}