﻿using DG.Tweening;
using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
//using IAP;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Setting_API : Singleton<Setting_API>
{

    //public static Setting_API Instance;

    // API url
    string url = "/api/setting";

    // resulting JSON from an API request
    public JSONNode jsonResult;
    public int Local_appVersion_code = 1;
    string rawJson;

    //public bool success;
    public int Total_lvl_server;
    public long fb_coin;
    public long guest_coin;
    public long credits;
    public long currentxp;
    public int app_version;
    public int is_undermaintenance;
    public string app_update_message;
    public string android_app_link;
    public string ios_app_link;
    public string privacy_policy;
    public string app_message;
    public string fb_share_link_title;
    public string share_private_link_message;
    public string terms_and_conditions;
    public int watch_video_coin;
    public bool is_daily_bonus;


    public string bit_coin_value;
    public int cashout_coin_value;
    public bool is_cashout_done;

    public bool req_call = true;

    public string updated_level;

    //userdata
    public string Username;
    public string profile_pic_link;
    public long wallet_coin;
    public int current_xp;
    public int is_unlocked = 0;
    public int is_blocked = 0;
    public int start;
    public int end;
    public string token;


    public int level;
    public int goldcoin;
    public float diamond;


    public float diamond_limit;
    public int coin_limit;


    [Space]
    [Header("FF")]
    [Space]
    [Header("Redeem")]
    //redeem
    public string[] name_redeem_FF;
    public string[] desc_redeem_FF;
    public string[] type_redeem_FF;
    public string[] frag_redeem_FF;
    public string[] frag_img_redeem_FF;
    public string[] img_redeem_FF;
    public string[] product_id_redeem_FF;

    [Space]
    [Header("MLBB")]
    //redeem
    public string[] name_redeem_MLBB;
    public string[] desc_redeem_MLBB;
    public string[] type_redeem_MLBB;
    public string[] frag_redeem_MLBB;
    public string[] frag_img_redeem_MLBB;
    public string[] img_redeem_MLBB;
    public string[] product_id_redeem_MLBB;

    [Space]
    [Header("ROB")]
    //redeem
    public string[] name_redeem_ROB;
    public string[] desc_redeem_ROB;
    public string[] type_redeem_ROB;
    public string[] frag_redeem_ROB;
    public string[] frag_img_redeem_ROB;
    public string[] img_redeem_ROB;
    public string[] product_id_redeem_ROB;



    [Space]
    [Header("FF")]
    [Space]
    [Header("Withdraw")]
    //redeem
    public string[] dia_withdraw_FF;
    public string[] game_dia_withdraw_FF;
    public string[] product_id_withdraw_FF;
    public string[] coin_img_withdraw_FF;
    public string[] type_withdraw_FF;
    public withdraw_type[] coin_type_FF;


    [Space]
    [Header("MLBB")]
    //redeem
    public string[] dia_withdraw_MLBB;
    public string[] game_dia_withdraw_MLBB;
    public string[] product_id_withdraw_MLBB;
    public string[] coin_img_withdraw_MLBB;
    public string[] type_withdraw_MLBB;
    public withdraw_type[] coin_type_MLBB;
    [Space]
    [Header("ROB")]
    //redeem
    public string[] dia_withdraw_ROB;
    public string[] game_dia_withdraw_ROB;
    public string[] product_id_withdraw_ROB;
    public string[] coin_img_withdraw_ROB;
    public string[] type_withdraw_ROB;
    public withdraw_type[] coin_type_ROB;

    [Header("Gamelist")]
    public List<string> GameName = new List<string>();
    public List<string> Bg_img_panel = new List<string>();
    public List<string> icon_panel = new List<string>();
    public List<string> icon_start_panel = new List<string>();


    private void Awake()
    {

    }


    private void Start()
    {
    }
    public void Get_settings_data()
    {
        //Shop_API_script.Instance.Get_settings_data();
        //Debug.Log("getdata");
        StartCoroutine("GetData");

    }



    // sends an API request - returns a JSON file
    void Manual_Start()
    {

        Debug.Log(rawJson);
        jsonResult = JSON.Parse(rawJson);
        Set_Json_data();
        Invoke("temp_waiter", 0.5f);


    }
    public void temp_waiter()
    {
        if (is_undermaintenance == 0)
        {
            if (Version_check())
            {
                //temp_before_login_start
                if (is_blocked == 0)
                {
                    Normal_start();
                }
            }
            else
            {
                UPdate_app_notice();
            }
        }
        else
        {
            Undermaintenance_Notice();
        }
    }


    public void Set_Json_data()
    {

        is_undermaintenance = jsonResult["data"]["app_config"]["is_undermaintenance"].AsInt;
        app_message = jsonResult["data"]["app_config"]["app_message"].Value;
        android_app_link = jsonResult["data"]["app_config"]["android_app_link"].Value;
        ios_app_link = jsonResult["data"]["app_config"]["ios_app_link"].Value;
        privacy_policy = jsonResult["data"]["app_config"]["privacy_policy"].Value;
        terms_and_conditions = jsonResult["data"]["app_config"]["privacy_policy"].Value;
        fb_share_link_title = jsonResult["data"]["app_config"]["fb_share_link_title"].Value;
        watch_video_coin = jsonResult["data"]["app_config"]["watch_video_coin"].AsInt;
        bit_coin_value = jsonResult["data"]["app_config"]["bit_coin_value"].Value;
        cashout_coin_value = jsonResult["data"]["app_config"]["cashout_coin_value"].AsInt;


        //coinLimit
        coin_limit = jsonResult["data"]["app_config"]["coin_limit"].AsInt;
        diamond_limit = jsonResult["data"]["app_config"]["diamond_limit"].AsFloat;

        //userDATA
        is_blocked = jsonResult["data"]["user"]["is_blocked"].AsInt;
        Username = jsonResult["data"]["user"]["name"].Value;

        //PlayerPrefs.SetString("Username", Username);
        goldcoin = jsonResult["data"]["user"]["goldcoin"].AsInt;
        Coin_Manager.instance.GoldCoin = goldcoin;
        diamond = jsonResult["data"]["user"]["diamond"].AsFloat;
        Coin_Manager.instance.Diamond = diamond;
        is_cashout_done = jsonResult["data"]["user"]["is_cashout_done"].AsBool;

        is_unlocked = jsonResult["data"]["user"]["is_unlocked"].AsInt;

        //int m = 0;
        //foreach (JSONNode message in jsonResult["data"]["gamelist"])
        //{
        //    m++;
        //}

        //GameName = new string[m];
        //Bg_img_panel = new string[m];
        //icon_panel = new string[m];
        //icon_start_panel = new string[m];

        if (is_blocked == 0)
        {
            //Set_User_data.Instance.setdata_after_get();



            int counter = 0;
            foreach (JSONNode message in jsonResult["data"]["gamelist"])
            {
                GameName.Add(message["game_name"].Value);
                Bg_img_panel.Add(message["bg_img"].Value);
                icon_panel.Add(message["thumb_nail"].Value);
                icon_start_panel.Add(message["icon"].Value);
                counter++;
            }

            bool a1 = GameName.Contains(PlayerPrefs.GetString("Game0", string.Empty)) ? true : false;
            bool a2 = GameName.Contains(PlayerPrefs.GetString("Game1", string.Empty)) ? true : false;
            bool a3 = GameName.Contains(PlayerPrefs.GetString("Game2", string.Empty)) ? true : false;

            if (a1 && a2 && a3)
            {
                Generate_redeem_diamond_panel();
                GameManager.Instance.game_start = true;
            }
            else
            {
                Canvas_singleton_script.Instance.GameSelection_panel.GetComponent<Game_selection_panel>().Generate_game_list();
                Canvas_singleton_script.Instance.GameSelection_panel.GetComponent<Game_selection_panel>().Preselect_fun();
                Canvas_singleton_script.Instance.GameSelection_panel.SetActive(true);
            }
            Splash_panel_script.instance.slider.DOFillAmount(1, 3f);
        }
        else if (is_blocked == 1)
        {
            //Notice_panel_script.Instance.notice_Panel.SetActive(true);
            Notice_panel_script.Instance.Blocked_panel.SetActive(true);
        }
        //skin_redeem();
        //Diamond_withdraw();

    }

    public void Generate_redeem_diamond_panel()
    {
        skin_redeem(PlayerPrefs.GetString("Game0"), PlayerPrefs.GetString("Game1"), PlayerPrefs.GetString("Game2"));
        Diamond_withdraw(PlayerPrefs.GetString("Game0"), PlayerPrefs.GetString("Game1"), PlayerPrefs.GetString("Game2"));
        Canvas_singleton_script.Instance.Redeem_panel.GetComponent<Redeem_panel_script>().generate_but();
        Canvas_singleton_script.Instance.Withdraw_panel.GetComponent<Withdraw_panel_script>().generate_but();
    }

    private void skin_redeem(string g1, string g2, string g3)
    {

        //freefire redeem
        int m = 0;
        foreach (JSONNode message in jsonResult["data"]["redeem"][g1])
        {
            m++;
        }

        name_redeem_FF = new string[m];
        desc_redeem_FF = new string[m];
        type_redeem_FF = new string[m];
        frag_redeem_FF = new string[m];
        frag_img_redeem_FF = new string[m];
        img_redeem_FF = new string[m];
        product_id_redeem_FF = new string[m];

        int counter = 0;
        foreach (JSONNode message in jsonResult["data"]["redeem"][g1])
        {

            name_redeem_FF[counter] = message["name"].Value;
            desc_redeem_FF[counter] = message["description"].Value;
            type_redeem_FF[counter] = message["type"].Value;
            frag_redeem_FF[counter] = message["fragment"].Value;
            frag_img_redeem_FF[counter] = message["images"].Value;
            img_redeem_FF[counter] = message["skin_icon"].Value;
            product_id_redeem_FF[counter] = message["_id"].Value;
            counter++;
        }


        //mlbb redeem
        m = 0;
        foreach (JSONNode message in jsonResult["data"]["redeem"][g2])
        {
            m++;
        }

        name_redeem_MLBB = new string[m];
        desc_redeem_MLBB = new string[m];
        type_redeem_MLBB = new string[m];
        frag_redeem_MLBB = new string[m];
        frag_img_redeem_MLBB = new string[m];
        img_redeem_MLBB = new string[m];
        product_id_redeem_MLBB = new string[m];

        counter = 0;
        foreach (JSONNode message in jsonResult["data"]["redeem"][g2])
        {

            name_redeem_MLBB[counter] = message["name"].Value;
            desc_redeem_MLBB[counter] = message["description"].Value;
            type_redeem_MLBB[counter] = message["type"].Value;
            frag_redeem_MLBB[counter] = message["fragment"].Value;
            frag_img_redeem_MLBB[counter] = message["images"].Value;
            img_redeem_MLBB[counter] = message["skin_icon"].Value;
            product_id_redeem_MLBB[counter] = message["_id"].Value;
            counter++;
        }


        //roblox redeem
        m = 0;
        foreach (JSONNode message in jsonResult["data"]["redeem"][g3])
        {
            m++;
        }

        name_redeem_ROB = new string[m];
        desc_redeem_ROB = new string[m];
        type_redeem_ROB = new string[m];
        frag_redeem_ROB = new string[m];
        frag_img_redeem_ROB = new string[m];
        img_redeem_ROB = new string[m];
        product_id_redeem_ROB = new string[m];

        counter = 0;
        foreach (JSONNode message in jsonResult["data"]["redeem"][g3])
        {

            name_redeem_ROB[counter] = message["name"].Value;
            desc_redeem_ROB[counter] = message["description"].Value;
            type_redeem_ROB[counter] = message["type"].Value;
            frag_redeem_ROB[counter] = message["fragment"].Value;
            frag_img_redeem_ROB[counter] = message["images"].Value;
            img_redeem_ROB[counter] = message["skin_icon"].Value;
            product_id_redeem_ROB[counter] = message["_id"].Value;
            counter++;
        }

        //Canvas_singleton_script.Instance.Redeem_panel.GetComponent<Redeem_panel_script>().generate_but();


    }
    private void Diamond_withdraw(string g1, string g2, string g3)
    {

        //freefire withdraw
        int m = 0;
        foreach (JSONNode message in jsonResult["data"]["withdrow"][g1])
        {
            m++;
        }

        dia_withdraw_FF = new string[m];
        game_dia_withdraw_FF = new string[m];
        product_id_withdraw_FF = new string[m];
        coin_img_withdraw_FF = new string[m];
        type_withdraw_FF = new string[m];
        coin_type_FF = new withdraw_type[m];


        int counter = 0;
        foreach (JSONNode message in jsonResult["data"]["withdrow"][g1])
        {

            dia_withdraw_FF[counter] = message["diamondvalue"].Value;
            game_dia_withdraw_FF[counter] = message["gamediamondvalue"].Value;
            product_id_withdraw_FF[counter] = message["_id"].Value;
            coin_img_withdraw_FF[counter] = message["imgurl"].Value;
            type_withdraw_FF[counter] = message["type"].Value;
            coin_type_FF[counter] = (withdraw_type)System.Enum.Parse(typeof(withdraw_type), message["cointype"].Value);
            counter++;
        }


        //mlbb withdrow
        m = 0;
        foreach (JSONNode message in jsonResult["data"]["withdrow"][g2])
        {
            m++;
        }

        dia_withdraw_MLBB = new string[m];
        game_dia_withdraw_MLBB = new string[m];
        product_id_withdraw_MLBB = new string[m];
        coin_img_withdraw_MLBB = new string[m];
        type_withdraw_MLBB = new string[m];
        coin_type_MLBB = new withdraw_type[m];

        counter = 0;
        foreach (JSONNode message in jsonResult["data"]["withdrow"][g2])
        {

            dia_withdraw_MLBB[counter] = message["diamondvalue"].Value;
            game_dia_withdraw_MLBB[counter] = message["gamediamondvalue"].Value;
            product_id_withdraw_MLBB[counter] = message["_id"].Value;
            coin_img_withdraw_MLBB[counter] = message["imgurl"].Value;
            type_withdraw_MLBB[counter] = message["type"].Value;
            coin_type_MLBB[counter] = (withdraw_type)System.Enum.Parse(typeof(withdraw_type), message["cointype"].Value);
            counter++;
        }


        //roblox withdraw
        m = 0;
        foreach (JSONNode message in jsonResult["data"]["withdrow"][g3])
        {
            m++;
        }

        dia_withdraw_ROB = new string[m];
        game_dia_withdraw_ROB = new string[m];
        product_id_withdraw_ROB = new string[m];
        coin_img_withdraw_ROB = new string[m];
        type_withdraw_ROB = new string[m];
        coin_type_ROB = new withdraw_type[m];

        counter = 0;
        foreach (JSONNode message in jsonResult["data"]["withdrow"][g3])
        {

            dia_withdraw_ROB[counter] = message["diamondvalue"].Value;
            game_dia_withdraw_ROB[counter] = message["gamediamondvalue"].Value;
            product_id_withdraw_ROB[counter] = message["_id"].Value;
            coin_img_withdraw_ROB[counter] = message["imgurl"].Value;
            type_withdraw_ROB[counter] = message["type"].Value;
            coin_type_ROB[counter] = (withdraw_type)System.Enum.Parse(typeof(withdraw_type), message["cointype"].Value);
            counter++;
        }

       

    }

    public void Normal_start()
    {

        Notice_panel_script.Instance.Loading_panel.SetActive(false);

    }

    public void startup_purchase_card()
    {
        if (PlayerPrefs.GetInt("CurrentXP") > 10000)
        {
            //Gamemanager.Instance.turnon_startup_purchase_panel();
        }
    }

    //public void Manual_start_ALL_ADS()
    //{
    //    Gamemanager.Instance.ads_controller.GetComponent<AdsController>().Manual_Start();
    //    Gamemanager.Instance.ads_controller.GetComponent<Unity_Ads_script>().Manual_Start();
    //    Gamemanager.Instance.ads_controller.GetComponent<FB_ADS_script>().Manual_Start();

    //}

    public void SEt_all_ads_ids()
    {
        //Gamemanager.Instance.ads_controller.GetComponent<>
    }


    public bool Version_check()
    {
        app_version = jsonResult["data"]["app_config"]["app_version"].AsInt;
        //Debug.Log(Application.version);
        if (Local_appVersion_code >= app_version)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //public static bool IsConnected(string hostedURL = "http://www.google.com")
    //{
    //    try
    //    {
    //        string HtmlText = GetHtmlFromUri(hostedURL);
    //        if (HtmlText == "")
    //            return false;
    //        else
    //            return true;
    //    }
    //    catch (IOException ex)
    //    {
    //        return false;
    //    }
    //}

    IEnumerator GetData()
    {
        UnityWebRequest www = UnityWebRequest.Get(Firebase_custome_script.Instance.server_link + url);
        req_call = false;
        www.SetRequestHeader("Authorization", PlayerPrefs.GetString("Token"));
        token = PlayerPrefs.GetString("Token");
        yield return www.Send();
        //Debug.Log(www);
        if (www.isNetworkError)
        {
            Debug.Log(www.error);


            Check_Internet_connection();
            req_call = true;
        }
        else
        {
            close_Retry_panel();
            // Show results as text
            //Debug.Log(www.downloadHandler.text);
            rawJson = www.downloadHandler.text;
            Manual_Start();
            // Or retrieve results as binary data
            // byte[] results = www.downloadHandler.data;
        }
    }


    // Update is called once per frame
    void Update()
    {
        //if (req_call)
        //{
        //    StartCoroutine("GetData");
        //}
    }

    public void Undermaintenance_Notice()
    {
        //Notice_panel_script.Instance.notice_Panel.SetActive(true);
        Notice_panel_script.Instance.Undermaintenance_panel.GetComponentInChildren<Text>().text = app_message;
        Notice_panel_script.Instance.Undermaintenance_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Close_app());
        Notice_panel_script.Instance.Undermaintenance_panel.SetActive(true);

    }



    public void UPdate_app_notice()
    {
        //Notice_panel_script.Instance.notice_Panel.SetActive(true);
        Notice_panel_script.Instance.Update_Panel.GetComponentInChildren<Text>().text = app_message;
        android_app_link = jsonResult["data"]["app_config"]["android_app_link"].Value;
        Notice_panel_script.Instance.Update_Panel.GetComponentInChildren<Button>().onClick.AddListener(() => Update_app());
        Notice_panel_script.Instance.Update_Panel.SetActive(true);

    }
    public void Close_app()
    {
        Application.Quit();
    }

    public void Update_app()
    {

        print("called");
#if UNITY_ANDROID
        Application.OpenURL(android_app_link);
#elif UNITY_IOS
        Application.OpenURL(ios_app_link);
#else
        Application.OpenURL(android_app_link);
#endif
    }
    public void Check_Internet_connection()
    {
        StartCoroutine(Ck_net(isConnected =>
        {
            if (isConnected)
            {
                Debug.Log("Internet Available!");
                //return true;
                if (!Notice_panel_script.Instance.Loading_panel.activeSelf)
                {
                    //Notice_panel_script.Instance.notice_Panel.SetActive(true);
                    Notice_panel_script.Instance.Something_wentwrong_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Notice_panel_script.Instance.Something_wentwrong_panel.SetActive(true);
                }
            }
            else
            {
                Debug.Log("Internet Not Available");
                //return  false;
                if (!Notice_panel_script.Instance.Loading_panel.activeSelf)
                {
                    //Notice_panel_script.Instance.notice_Panel.SetActive(true);
                    Notice_panel_script.Instance.No_internet_Panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Notice_panel_script.Instance.No_internet_Panel.SetActive(true);
                }
            }
        }));
    }

    public IEnumerator Ck_net(Action<bool> syncResult)
    {
        const string echoServer = "http://google.com";

        bool result;
        using (var request = UnityWebRequest.Head(echoServer))
        {
            request.timeout = 0;
            yield return request.SendWebRequest();
            result = !request.isNetworkError && !request.isHttpError && request.responseCode == 200;
        }
        syncResult(result);
    }
    //public bool check_net()
    //{
    //    WWW www = new WWW("http://www.google.com");
    //    //yield return www;
    //    if (www.error != null)
    //    {
    //        return false;
    //    }
    //    else
    //    {
    //        return true;
    //    }

    //}
    public void Retry_connection()
    {
        Get_settings_data();
        Notice_panel_script.Instance.Something_wentwrong_panel.SetActive(false);
        Notice_panel_script.Instance.No_internet_Panel.SetActive(false);
        Notice_panel_script.Instance.Loading_panel.SetActive(true);
        StartCoroutine("Retry_connection_coroutine");
    }
    IEnumerator Retry_connection_coroutine()
    {
        yield return new WaitForSecondsRealtime(3f);
        //StartCoroutine("check_connection");
        Notice_panel_script.Instance.Loading_panel.SetActive(false);
        //Notice_panel_script.Instance.notice_Panel.SetActive(false);
        Get_settings_data();
    }
    public void close_Retry_panel()
    {
        Notice_panel_script.Instance.Something_wentwrong_panel.SetActive(false);
        Notice_panel_script.Instance.No_internet_Panel.SetActive(false);
        Notice_panel_script.Instance.Loading_panel.SetActive(false);
        //Notice_panel_script.Instance.notice_Panel.SetActive(false);
    }


    //public void Check_updated_lvl()
    //{
    //    if (PlayerPrefs.GetString("updated_level", "0") == updated_level)
    //    {

    //    }
    //    else
    //    {
    //        string full_str = updated_level;
    //        string[] az = full_str.Split(',');
    //        foreach (string a in az)
    //        {
    //            FileChk_lvl(a);
    //        }
    //        PlayerPrefs.SetString("updated_level", updated_level);
    //    }
    //}

    public void FileChk_lvl(string lvl_name)
    {
        if (!Directory.Exists(Application.persistentDataPath + "/Levels"))
        {
            //if it doesn't, create it
            Directory.CreateDirectory(Application.persistentDataPath + "/Levels");

        }

        else
        {
            string filePath = Application.persistentDataPath + "/Levels/" + lvl_name + ".lvl";

            if (System.IO.File.Exists(filePath))
            {
                // The file exists -> run event
                File.Delete(filePath);
            }
            else
            {
                // The file does not exist -> run event
            }
        }
    }

    


    protected override void OnApplicationQuitCallback()
    {
        //throw new NotImplementedException();
    }

    protected override void OnEnableCallback()
    {
        //throw new NotImplementedException();
    }
}




