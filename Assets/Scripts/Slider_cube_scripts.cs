using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Slider_cube_scripts : MonoBehaviour
{
    public static Slider_cube_scripts instance;
    public GameObject cube_img_;
    public GameObject coin_float;

    public Sprite[] sprites_cubes;
    public List<GameObject> cube_list;
    public Transform canvas;
    public float[] pos;

    public Sprite right_mark, diamond;


    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        Generate_cubes();
        //InvokeRepeating("move_cubes",1,2f);
    }

    // Update is called once per frame
    void Update()
    {

    }



    public void Generate_cubes()
    {
        if (cube_list.Count == 0)
        {
            cube_list = new List<GameObject>();
        }
        int mf = (int)(Mathf.Log(PlayerPrefs.GetInt("Max_num", 8), 2)) - 3;
        Debug.Log(mf);
        for (int i = 0; i < 4; i++)
        {

            if (mf == 0)
            {
                GetComponent<Image>().fillAmount = 0;
            }
            else
            {
                GetComponent<Image>().fillAmount = 0.35f;
            }
            GameObject neww = Instantiate(cube_img_);
            neww.transform.SetParent(this.transform);
            neww.transform.localScale = Vector3.one;
            neww.transform.localPosition = new Vector3(pos[i], 0, 0);

            if (mf > 2)
            {
                int n = mf - 2;
                neww.GetComponent<Image>().sprite = sprites_cubes[n + i];
            }
            else
            {
                int n = 0;
                neww.GetComponent<Image>().sprite = sprites_cubes[n + i];
            }//if()
            cube_list.Add(neww);
            //move_cubes();
        }

        if (mf >= 2)
        {
            disable_cube(cube_list[0],true);
            enable_cube(cube_list[1]);
            disable_cube(cube_list[2]);
            disable_cube(cube_list[3]);
            GetComponent<Image>().DOFillAmount(0.35f, 1f).SetUpdate(true);
        }
        else if (mf < 2)
        {
            if (mf == 1)
            {
                enable_cube(cube_list[0]);
                disable_cube(cube_list[1]);
                disable_cube(cube_list[2]);
                disable_cube(cube_list[3]);
            }
        }
        //move_cubes();
    }

    public void move_cube_inv()
    {
        Invoke("move_cubes", 0.5f);
    }

    public void move_cubes()
    {
        int mf = (int)(Mathf.Log(PlayerPrefs.GetInt("Max_num", 8), 2)) - 3;
        if (mf == 2)
        {
            disable_cube(cube_list[0]);
            enable_cube(cube_list[1]);
            Collect_diamond(cube_list[1]);
            disable_cube(cube_list[2]);
            disable_cube(cube_list[3]);
            GetComponent<Image>().DOFillAmount(0.35f, 1f).SetUpdate(true);
        }
        else if (mf < 2)
        {
            if (mf == 1)
            {
                enable_cube(cube_list[0]);
                Collect_diamond(cube_list[0]);
                disable_cube(cube_list[1]);
                disable_cube(cube_list[2]);
                disable_cube(cube_list[3]);
            }
        }
        else
        {
            GameObject a = cube_list[0];
            cube_list[0].transform.DOScale(Vector3.zero, 0.2f);
            cube_list[0].transform.GetComponent<CanvasGroup>().DOFade(0, 0.2f).OnComplete(() => Destroy(a));

            cube_list[0] = cube_list[1];
            cube_list[0].transform.DOLocalMoveX(pos[0], 0.2f);
            cube_list[0].transform.DOScale(Vector3.one, 0.2f);
            disable_cube(cube_list[0]);

            cube_list[1] = cube_list[2];
            cube_list[1].transform.DOLocalMoveX(pos[1], 0.2f);
            cube_list[1].transform.DOScale(new Vector3(1.2f, 1.2f, 1.2f), 0.2f);
            enable_cube(cube_list[1]);
            Collect_diamond(cube_list[1]);

            cube_list[2] = cube_list[3];
            cube_list[2].transform.DOLocalMoveX(pos[2], 0.2f);
            disable_cube(cube_list[2]);

            cube_list[3] = Instantiate(cube_img_);
            cube_list[3].transform.SetParent(this.transform);
            cube_list[3].transform.localScale = Vector3.zero;
            cube_list[3].transform.localPosition = new Vector3(pos[3], 0, 0);
            cube_list[3].transform.GetComponent<Image>().sprite = sprites_cubes[mf + 3];
            cube_list[3].transform.DOScale(Vector3.one, 0.2f);
            disable_cube(cube_list[3]);
        }
    }

    private void Collect_diamond(GameObject asdf)
    {
        int axs = Random.Range(5, 10);
        for (int i = 0; i < axs; i++)
        {

            GameObject a = Instantiate(coin_float, asdf.transform.position, Quaternion.identity);
            a.transform.SetParent(this.gameObject.transform);
            Vector3 rannn= Random.insideUnitCircle/20;
            a.transform.position = asdf.transform.position + rannn;
            a.transform.rotation = Quaternion.identity;
            a.transform.localScale = Vector3.one;
            a.GetComponent<Float_obj_to_target_script>().targetPos = Coin_Manager.instance.diam_top_pos.position;
        }
        Coin_Manager.instance.AddDiamond(Coin_Manager.instance.Random_num_diamond());
    }

    public void disable_cube(GameObject am)
    {
        Color cl = am.GetComponent<Image>().color;
        cl.a = 0.5f;
        am.GetComponent<Image>().color = cl;
    }
    public void disable_cube(GameObject am, bool t)
    {
        Color cl = am.GetComponent<Image>().color;
        cl.a = 0.5f;
        am.GetComponent<Image>().color = cl;
        am.transform.GetChild(0).GetComponent<Image>().sprite = right_mark;
    }

    public void enable_cube(GameObject am)
    {
        Color cl = am.GetComponent<Image>().color;
        cl.a = 1f;
        am.GetComponent<Image>().color = cl;
        am.transform.GetChild(0).GetComponent<Image>().sprite = right_mark;
    }
}
