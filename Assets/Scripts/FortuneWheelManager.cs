﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class FortuneWheelManager : MonoBehaviour
{
    private bool _isStarted;
    private float[] _sectorsAngles;
    private float _finalAngle;
    private float _startAngle = 0;
    private float _currentLerpRotationTime;
    public Button TurnButton;
    public GameObject Circle; 			// Rotatable Object with rewards
    //public Text CoinsDeltaText;
    public Text InitialCoinsText; // Pop-up text with wasted or rewarded coins amount
    public Text RewardCoinsText; 		// Pop-up text with wasted or rewarded coins amount
    public int TurnCost = 300;			// How much coins user waste when turn whe wheel
    public float CurrentCoinsAmount = 1000;	// Started coins amount. In your project it can be set up from CoinsManager or from PlayerPrefs and so on
    public float PreviousCoinsAmount;		// For wasted coins animation
    //public Text Initial_value;
    public coin_type ct;

    //public GameObject win_panel;
    public GameObject nothanks_but;
    public Image coin_type_img;


    public Sprite gold_coin, diamond;

    public int gold_value;
    public float diamond_value;


    float rewrd_mul = 0;
    private void Awake()
    {
        PreviousCoinsAmount = CurrentCoinsAmount;
        TurnButton.interactable = false;
        //CurrentCoinsText.text = CurrentCoinsAmount.ToString ();
    }


    void OnEnable()
    {
        //GameManager.Instance.stop_time_no_ads();
        //TurnButton.interactable = false;
        TurnButton.interactable = true;
        nothanks_but.SetActive(false);
        diamond_value = 0;
        gold_value = 0;
        RewardCoinsText.text = "?";
        if (ct == coin_type.Diamond)
        {
            diamond_value = Coin_Manager.instance.Random_num_diamond();
            coin_type_img.sprite = diamond;
            InitialCoinsText.text = diamond_value.ToString();
        }
        else
        {
            if (Coin_Manager.instance.GoldCoin >= Setting_API.Instance.coin_limit)
            {
                gold_value = 1;
            }
            else
            {
                gold_value = 300000;
            }
            coin_type_img.sprite = gold_coin;
            InitialCoinsText.text = AbbrevationUtility.AbbreviateNumber(gold_value);
        }


        rewrd_mul = 0;
        //TurnButton.interactable = true;
        StartCoroutine("ststst");
    }

    IEnumerator ststst()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        TurnButton.interactable = true;
        StartCoroutine("thnkkksksksks");
        //TurnWheel();

        //yield return new WaitForSecondsRealtime(0.5f);
        //nothanks_but.SetActive(true);

    }
    public void TurnWheel()
    {
        AudioManager.Instance.Spin_start();
        // Player has enough money to turn the wheel
        if (CurrentCoinsAmount >= TurnCost)
        {
            _currentLerpRotationTime = 0f;

            // Fill the necessary angles (for example if you want to have 12 sectors you need to fill the angles with 30 degrees step)
            _sectorsAngles = new float[] { 30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330, 360 };

            int fullCircles = 5;
            float randomFinalAngle = _sectorsAngles[UnityEngine.Random.Range(0, _sectorsAngles.Length)];

            // Here we set up how many circles our wheel should rotate before stop
            _finalAngle = -(fullCircles * 360 + randomFinalAngle);
            _isStarted = true;

            PreviousCoinsAmount = CurrentCoinsAmount;

            // Decrease money for the turn
            //CurrentCoinsAmount -= TurnCost;

            // Show wasted coins
            //CoinsDeltaText.text = "-" + TurnCost;
            //CoinsDeltaText.gameObject.SetActive (true);

            // Animate coins
            //StartCoroutine (HideCoinsDelta ());
            //StartCoroutine (UpdateCoinsAmount ());
        }
    }

    private void GiveAwardByAngle()
    {
        // Here you can set up rewards for every sector of wheel
        switch ((int)_startAngle)
        {
            case 0:
                RewardCoins(7.0f);
                break;
            case -330:
                RewardCoins(1.5f);
                break;
            case -300:
                RewardCoins(2.0f);
                break;
            case -270:
                RewardCoins(2.5f);
                break;
            case -240:
                RewardCoins(3.0f);
                break;
            case -210:
                RewardCoins(3.5f);
                break;
            case -180:
                RewardCoins(4.0f);
                break;
            case -150:
                RewardCoins(4.5f);
                break;
            case -120:
                RewardCoins(5.0f);
                break;
            case -90:
                RewardCoins(5.5f);
                break;
            case -60:
                RewardCoins(6.0f);
                break;
            case -30:
                RewardCoins(6.5f);
                break;
            default:
                RewardCoins(7.0f);
                break;
        }
    }

    void Update()
    {
        // Make turn button non interactable if user has not enough money for the turn
        if (_isStarted || CurrentCoinsAmount < TurnCost)
        {
            TurnButton.interactable = false;
            TurnButton.GetComponent<Image>().color = new Color(255, 255, 255, 0.5f);
        }
        else
        {
            TurnButton.interactable = true;
            TurnButton.GetComponent<Image>().color = new Color(255, 255, 255, 1);
        }

        if (!_isStarted)
            return;

        float maxLerpRotationTime = 4f;

        // increment timer once per frame
        _currentLerpRotationTime += Time.unscaledDeltaTime;
        if (_currentLerpRotationTime > maxLerpRotationTime || Circle.transform.eulerAngles.z == _finalAngle)
        {
            _currentLerpRotationTime = maxLerpRotationTime;
            _isStarted = false;
            _startAngle = _finalAngle % 360;

            GiveAwardByAngle();
            //StartCoroutine(HideCoinsDelta ());
        }

        // Calculate current position using linear interpolation
        float t = _currentLerpRotationTime / maxLerpRotationTime;

        // This formulae allows to speed up at start and speed down at the end of rotation.
        // Try to change this values to customize the speed
        t = t * t * t * (t * (6f * t - 15f) + 10f);
        if (t <= 0.1f)
        {
            AudioManager.Instance.spin_source.pitch = t * 10f;
        }
        else if (t < 0.9f)
        {
            AudioManager.Instance.spin_source.pitch = 1;
        }
        else if (t >= 0.9f)
        {
            AudioManager.Instance.spin_source.pitch = 1f - ((t * 10f) - 9f);
        }
        //Debug.Log("t" + AudioManager.Instance.spin_source.pitch);
        float angle = Mathf.Lerp(_startAngle, _finalAngle, t);
        Circle.transform.eulerAngles = new Vector3(0, 0, angle);
    }

    private void RewardCoins(float awardCoins)
    {
        AudioManager.Instance.Spin_stop();
        rewrd_mul = awardCoins;
        //CurrentCoinsAmount += awardCoins;
        if (ct == coin_type.Diamond)
        {
            RewardCoinsText.text = (awardCoins * diamond_value).ToString();
        }
        else if (ct == coin_type.Gold)
        {
            RewardCoinsText.text = AbbrevationUtility.AbbreviateNumber(gold_value + (int)(gold_value * rewrd_mul));
        }

        //TurnButton.gameObject.SetActive(true);
        //Coinget();


        //CoinsDeltaText.text = "+" + awardCoins;
        //CoinsDeltaText.gameObject.SetActive (true);
        StartCoroutine("coinngett");
    }

    IEnumerator coinngett()
    {
        yield return new WaitForSecondsRealtime(1f);
        Coinget();
    }

    private IEnumerator thnkkksksksks()
    {
        yield return new WaitForSecondsRealtime(2f);
        nothanks_but.SetActive(true);
        //CoinsDeltaText.gameObject.SetActive (false);
    }

    private IEnumerator UpdateCoinsAmount()
    {
        // Animation for increasing and decreasing of coins amount
        const float seconds = 0.5f;
        float elapsedTime = 0;

        while (elapsedTime < seconds)
        {
            RewardCoinsText.text = Mathf.Floor(Mathf.Lerp(PreviousCoinsAmount, CurrentCoinsAmount, (elapsedTime / seconds))).ToString();
            elapsedTime += Time.unscaledDeltaTime;

            yield return new WaitForEndOfFrame();
        }

        PreviousCoinsAmount = CurrentCoinsAmount;
        RewardCoinsText.text = CurrentCoinsAmount.ToString();
    }

    public void Onclick_free()
    {
        Ads_priority_script.Instance.watch_video(Ads_reward_type.SpinGift);
        nothanks_but.SetActive(false);
    }


    public void Video_reward()
    {
        GameManager.Instance.stop_time_no_ads();
        //Coinget();
        TurnWheel();
    }

    private void Coinget()
    {
        switch (ct)
        {
            case coin_type.Diamond:
                //RewardCoinsText.text = (diamond_value * rewrd_mul).ToString();
                diamond_value = diamond_value + (diamond_value * rewrd_mul);

                break;
            case coin_type.Gold:
                gold_value = gold_value + (int)(gold_value * rewrd_mul);

                break;
            default:
                break;
        }
        //win_panel.GetComponent<Win_panel_script>().Final_diamond.text =  (float.Parse(Initial_value.text) * rewrd_mul).ToString();
        //win_panel.SetActive(true);
        GameManager.Instance.start_time();
        Slider_cube_scripts.instance.move_cube_inv();
        Vector2 veca = new Vector2(Screen.width / 2, Screen.height / 2);
        Vector3 position = veca;
        position.z = (Controller_script.instance.coin_canvas.transform.position - Camera.main.transform.position).magnitude;
        Vector3 az = Camera.main.ScreenToWorldPoint(position);
        Debug.Log(az);
        if (ct == coin_type.Diamond)
        {
            Coin_Manager.instance.Collect_diamond(az);
            Coin_Manager.instance.AddDiamond(diamond_value);
        }
        else if (ct == coin_type.Gold)
        {
            Coin_Manager.instance.Collect_Gold_coin(az);
            Coin_Manager.instance.AddGoldCoin(gold_value);
        }
        gameObject.SetActive(false);
    }

    public void nothanks_but_click()
    {

        //win_panel.GetComponent<Win_panel_script>().Final_diamond.text = Initial_value.text;
        //win_panel.SetActive(true);
        gameObject.SetActive(false);
        GameManager.Instance.start_time();
        Slider_cube_scripts.instance.move_cube_inv();
        Vector2 veca = new Vector2(Screen.width / 2, Screen.height / 2);
        Vector3 position = veca;
        position.z = (Controller_script.instance.coin_canvas.transform.position - Camera.main.transform.position).magnitude;
        Vector3 az = Camera.main.ScreenToWorldPoint(position);
        Debug.Log(az);
        if (ct == coin_type.Diamond)
        {
            Coin_Manager.instance.Collect_diamond(az);
            Coin_Manager.instance.AddDiamond(diamond_value);
        }
        else if (ct == coin_type.Gold)
        {
            Coin_Manager.instance.Collect_Gold_coin(az);
            Coin_Manager.instance.AddGoldCoin(gold_value);
        }
    }
}
