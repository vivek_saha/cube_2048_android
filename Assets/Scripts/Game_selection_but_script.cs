using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Game_selection_but_script : MonoBehaviour
{


     public Button Game_but;
    public Image game_icon;
    public Text game_name_text;

    public GameObject Selected;

    public string game_icon_link;
    public string game_name;

    public Sprite[] game_icon_frame_sprite;
   

    void Start()
    {
        Deselected_fun();
        Download_image();
    }

    public void Deselected_fun()
    {
        Game_but.GetComponent<Image>().enabled = true;
        Game_but.GetComponent<Image>().sprite = game_icon_frame_sprite[0];
        Selected.SetActive(false);
    }

    public void Selected_fun()
    {
        Game_but.GetComponent<Image>().enabled= false;
        Selected.SetActive(true);
    }
    public void Download_image()
    {
        StartCoroutine(GetTexture());
    }

    IEnumerator GetTexture()
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(Firebase_custome_script.Instance.server_link + game_icon_link);
        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log(www.error);
        }
        else
        {
            Texture2D myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
            game_icon.sprite = Sprite.Create(myTexture, new Rect(0.0f, 0.0f, myTexture.width, myTexture.height), new Vector2(0.5f, 0.5f), 100.0f);
        }
    }



    public void add_listner()
    {
        Game_but.onClick.AddListener(()=>OnBut_click());
    }
    public void OnBut_click()
    {
        Game_selection_panel.instance.selection_but_Counter(this);
    }
}
