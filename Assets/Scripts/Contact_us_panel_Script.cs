﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Contact_us_panel_Script : MonoBehaviour
{

    public static Contact_us_panel_Script I;

    public GameObject NEw_msg_panel;
    public GameObject Already_generated_panel;
    public GameObject Loading_panel;

    public GameObject content;

    public GameObject Admin_msg_container, User_msg_container;

    public InputField Messg;





    public bool Message_verifier()
    {
        return Messg.text == null ? false : true;
    }

    public void Submit_message()
    {

        if (Message_verifier())
        {
            ContactUS_API.Instance.Message = Messg.text;
            ContactUS_API.Instance.is_message = 1;
            NEw_msg_panel.SetActive(false);
            Already_generated_panel.SetActive(true);
            ContactUS_API.Instance.Update_data();
        }
        else
        {
            _ShowAndroidToastMessage("Message Can't Be Empty!");
        }
    }



    private void _ShowAndroidToastMessage(string message)
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        if (unityActivity != null)
        {
            AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
            AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity, message, 0);
            toastObject.Call("show");
        }
    }


    public void OnContactus_but_click()
    {
        if (PlayerPrefs.GetInt("1st_time", 0) == 0)
        {
            NEw_msg_panel.SetActive(true);
            //Already_generated_panel.SetActive(true);
            //ContactUS_API.Instance.Update_data();
        }
        else
        {
            //NEw_msg_panel.SetActive(false);
            Already_generated_panel.SetActive(true);
            ContactUS_API.Instance.Update_data();
        }
    }

    public void Null_text_message()
    {
        Messg.text = string.Empty;
    }

    private void Awake()
    {
        I = this;
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
