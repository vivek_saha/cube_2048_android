﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Redeem_panel_script : MonoBehaviour
{
    public static Redeem_panel_script Instance;


    public GameObject skin_redeem_but;

    public GameObject mlbb_panel, freefire_panel, roblox_panel;

    public Image[] BG_img;

    public Image[] mlbb_panel_img;
    public Image[] ff_panel_img;
    public Image[] roblox_panel_img;

    //public Image mlbb_panel_bg, freefire_panel_bg, roblox_panel_bg;
    [Header("mlbb")]
    [Space]
    public GameObject mlbb_content;


    [Header("FreeFire")]
    [Space]
    public GameObject freefire_content;


    [Header("Roblox")]
    [Space]
    public GameObject roblox_content;



    private void Awake()
    {
        Instance = this;
    }

    public void Onclick_panel_change_(GameObject tp)
    {
        mlbb_panel.SetActive(false);
        freefire_panel.SetActive(false);
        roblox_panel.SetActive(false);
        tp.SetActive(true);
    }


    public void generate_but()
    {

        foreach (Transform a in freefire_content.transform)
        {
            Destroy(a.gameObject);
        }
        //ff
        for (int i = 0; i < Setting_API.Instance.product_id_redeem_FF.Length; i++)
        {
            GameObject temp = Instantiate(skin_redeem_but);
            temp.transform.SetParent(freefire_content.transform);
            temp.transform.localScale = Vector3.one;
            
            temp.GetComponent<skin_redeem_but_script>().Skin_Name.text = Setting_API.Instance.name_redeem_FF[i];
            temp.GetComponent<skin_redeem_but_script>().Skin_description.text = Setting_API.Instance.desc_redeem_FF[i];
            temp.GetComponent<skin_redeem_but_script>().product_id = Setting_API.Instance.product_id_redeem_FF[i];
            temp.GetComponent<skin_redeem_but_script>().skinImage_link = Setting_API.Instance.img_redeem_FF[i];
            StartCoroutine(GetTextureRequest_ff(Firebase_custome_script.Instance.server_link + Setting_API.Instance.img_redeem_FF[i], (response) => {
                temp.GetComponent<skin_redeem_but_script>().SkinImage.sprite = response;
            }));
            temp.GetComponent<skin_redeem_but_script>().frag_limit = int.Parse(Setting_API.Instance.frag_redeem_FF[i]);
            temp.GetComponent<skin_redeem_but_script>().fragment_sprite_link = Setting_API.Instance.frag_img_redeem_FF[i];
            StartCoroutine(GetTextureRequest_ff_frag(Firebase_custome_script.Instance.server_link + Setting_API.Instance.frag_img_redeem_FF[i], (response) => {
                temp.GetComponent<skin_redeem_but_script>().Fragement_sprite = response;
            }));
                    
            player_prefs_adder_checker(Setting_API.Instance.name_redeem_FF[i]);
            temp.GetComponent<skin_redeem_but_script>().set_fragment_fill();
            //temp.GetComponent<skin_redeem_but_script>().Download_image();
            temp.GetComponent<skin_redeem_but_script>().button_Click();
        }


        foreach (Transform a in mlbb_content.transform)
        {
            Destroy(a.gameObject);
        }

        ////MLBB
        for (int i = 0; i < Setting_API.Instance.product_id_redeem_MLBB.Length; i++)
        {
            GameObject temp = Instantiate(skin_redeem_but);
            temp.transform.SetParent(mlbb_content.transform);
            temp.transform.localScale = Vector3.one;
           
            temp.GetComponent<skin_redeem_but_script>().Skin_Name.text = Setting_API.Instance.name_redeem_MLBB[i];
            temp.GetComponent<skin_redeem_but_script>().Skin_description.text = Setting_API.Instance.desc_redeem_MLBB[i];
            temp.GetComponent<skin_redeem_but_script>().product_id = Setting_API.Instance.product_id_redeem_MLBB[i];
            temp.GetComponent<skin_redeem_but_script>().skinImage_link = Setting_API.Instance.img_redeem_MLBB[i];
            StartCoroutine(GetTextureRequest_mlbb(Firebase_custome_script.Instance.server_link + Setting_API.Instance.img_redeem_MLBB[i], (response) => {
                temp.GetComponent<skin_redeem_but_script>().SkinImage.sprite = response;
            }));
            temp.GetComponent<skin_redeem_but_script>().frag_limit = int.Parse(Setting_API.Instance.frag_redeem_MLBB[i]);
            temp.GetComponent<skin_redeem_but_script>().fragment_sprite_link = Setting_API.Instance.frag_img_redeem_MLBB[i];
            StartCoroutine(GetTextureRequest_mlbb_frag(Firebase_custome_script.Instance.server_link + Setting_API.Instance.frag_img_redeem_MLBB[i], (response) => {
                temp.GetComponent<skin_redeem_but_script>().Fragement_sprite = response;
            }));

            player_prefs_adder_checker(Setting_API.Instance.name_redeem_MLBB[i]);
            temp.GetComponent<skin_redeem_but_script>().set_fragment_fill();
            //temp.GetComponent<skin_redeem_but_script>().Download_image();
            temp.GetComponent<skin_redeem_but_script>().button_Click();
        }


        foreach (Transform a in roblox_content.transform)
        {
            Destroy(a.gameObject);
        }

        ////Roblox
        for (int i = 0; i < Setting_API.Instance.product_id_redeem_ROB.Length; i++)
        {
            GameObject temp = Instantiate(skin_redeem_but);
            temp.transform.SetParent(roblox_content.transform);
            temp.transform.localScale = Vector3.one;
 
            temp.GetComponent<skin_redeem_but_script>().Skin_Name.text = Setting_API.Instance.name_redeem_ROB[i];
            temp.GetComponent<skin_redeem_but_script>().Skin_description.text = Setting_API.Instance.desc_redeem_ROB[i];
            temp.GetComponent<skin_redeem_but_script>().product_id = Setting_API.Instance.product_id_redeem_ROB[i];
            temp.GetComponent<skin_redeem_but_script>().skinImage_link = Setting_API.Instance.img_redeem_ROB[i];
            StartCoroutine(GetTextureRequest_rob(Firebase_custome_script.Instance.server_link + Setting_API.Instance.img_redeem_ROB[i], (response) => {
                temp.GetComponent<skin_redeem_but_script>().SkinImage.sprite = response;
            }));
            temp.GetComponent<skin_redeem_but_script>().frag_limit = int.Parse(Setting_API.Instance.frag_redeem_ROB[i]);
            temp.GetComponent<skin_redeem_but_script>().fragment_sprite_link = Setting_API.Instance.frag_img_redeem_ROB[i];
            StartCoroutine(GetTextureRequest_rob_frag(Firebase_custome_script.Instance.server_link + Setting_API.Instance.frag_img_redeem_ROB[i], (response) => {
                temp.GetComponent<skin_redeem_but_script>().Fragement_sprite = response;
            }));

            player_prefs_adder_checker(Setting_API.Instance.name_redeem_FF[i]);
            temp.GetComponent<skin_redeem_but_script>().set_fragment_fill();
            //temp.GetComponent<skin_redeem_but_script>().Download_image();
            temp.GetComponent<skin_redeem_but_script>().button_Click();
        }

        icon_panel_download();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }



    public void player_prefs_adder_checker(string KeyName)
    {
        if (PlayerPrefs.HasKey(KeyName))
        {

            //Debug.Log("The key " + KeyName + " exists");
        }
        else
        {

            PlayerPrefs.SetInt(KeyName, 0);
            //Debug.Log("The key " + KeyName + " does not exist");
        }
    }


    private void icon_panel_download()
    {
        int be = Setting_API.Instance.GameName.IndexOf(Setting_API.Instance.type_withdraw_MLBB[0]);
        //Debug.Log()
        StartCoroutine(GetTextureRequest(Firebase_custome_script.Instance.server_link + Setting_API.Instance.icon_panel[be], (response) =>
        {
            mlbb_panel_img[0].sprite = response;
            mlbb_panel_img[1].sprite = response;
            mlbb_panel_img[2].sprite = response;
            //foreach (Image am in mlbb_panel_img)
            //{
            //    am.sprite = response;
            //}
        }));
        StartCoroutine(GetTextureRequest(Firebase_custome_script.Instance.server_link + Setting_API.Instance.Bg_img_panel[be], (response) =>
        {

            BG_img[0].sprite = response;

        }));
        //panel_icon
        int re = Setting_API.Instance.GameName.IndexOf(Setting_API.Instance.type_withdraw_FF[0]);

        StartCoroutine(GetTextureRequest(Firebase_custome_script.Instance.server_link + Setting_API.Instance.icon_panel[re], (response) =>
        {

            ff_panel_img[0].sprite = response;
            ff_panel_img[1].sprite = response;
            ff_panel_img[2].sprite = response;
            //foreach (Image am in ff_panel_img)
            //{
            //    am.sprite = response;
            //}
        }));
        StartCoroutine(GetTextureRequest(Firebase_custome_script.Instance.server_link + Setting_API.Instance.Bg_img_panel[re], (response) =>
        {

            BG_img[2].sprite = response;

        }));



        //panel_icon


        //panel_icon
        int xe = Setting_API.Instance.GameName.IndexOf(Setting_API.Instance.type_withdraw_ROB[0]);

        StartCoroutine(GetTextureRequest(Firebase_custome_script.Instance.server_link + Setting_API.Instance.icon_panel[xe], (response) =>
        {
            roblox_panel_img[0].sprite = response;
            roblox_panel_img[1].sprite = response;
            roblox_panel_img[2].sprite = response;
            //foreach (Image am in roblox_panel_img)
            //{
            //    am.sprite = response;
            //}
        }));
        StartCoroutine(GetTextureRequest(Firebase_custome_script.Instance.server_link + Setting_API.Instance.Bg_img_panel[xe], (response) =>
        {

            BG_img[1].sprite = response;

        }));

    }

    IEnumerator GetTextureRequest(string url, System.Action<Sprite> callback)
    {
        using (var www = UnityWebRequestTexture.GetTexture(url))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    var texture = DownloadHandlerTexture.GetContent(www);
                    var rect = new Rect(0, 0, texture.width, texture.height);
                    var sprite = Sprite.Create(texture, rect, new Vector2(0.5f, 0.5f), 100.0f);
                    callback(sprite);
                }
            }
        }
    }






    IEnumerator GetTextureRequest_panel_icon(string url, System.Action<Sprite> callback)
    {
        using (var www = UnityWebRequestTexture.GetTexture(url))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    var texture = DownloadHandlerTexture.GetContent(www);
                    var rect = new Rect(0, 0, texture.width, texture.height);
                    var sprite = Sprite.Create(texture, rect, new Vector2(0.5f, 0.5f), 100.0f);
                    callback(sprite);
                }
            }
        }
    }


    IEnumerator GetTextureRequest_bg(string url, System.Action<Sprite> callback)
    {
        using (var www = UnityWebRequestTexture.GetTexture(url))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    var texture = DownloadHandlerTexture.GetContent(www);
                    var rect = new Rect(0, 0, texture.width, texture.height);
                    var sprite = Sprite.Create(texture, rect, new Vector2(0.5f, 0.5f), 100.0f);
                    callback(sprite);
                }
            }
        }
    }
    IEnumerator GetTextureRequest_mlbb(string url, System.Action<Sprite> callback)
    {
        using (var www = UnityWebRequestTexture.GetTexture(url))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    var texture = DownloadHandlerTexture.GetContent(www);
                    var rect = new Rect(0, 0, texture.width, texture.height);
                    var sprite = Sprite.Create(texture, rect, new Vector2(0.5f, 0.5f), 100.0f);
                    callback(sprite);
                }
            }
        }
    }
    IEnumerator GetTextureRequest_ff(string url, System.Action<Sprite> callback)
    {
        using (var www = UnityWebRequestTexture.GetTexture(url))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    var texture = DownloadHandlerTexture.GetContent(www);
                    var rect = new Rect(0, 0, texture.width, texture.height);
                    var sprite = Sprite.Create(texture, rect, new Vector2(0.5f, 0.5f), 100.0f);
                    callback(sprite);
                }
            }
        }
    }
    IEnumerator GetTextureRequest_rob(string url, System.Action<Sprite> callback)
    {
        using (var www = UnityWebRequestTexture.GetTexture(url))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    var texture = DownloadHandlerTexture.GetContent(www);
                    var rect = new Rect(0, 0, texture.width, texture.height);
                    var sprite = Sprite.Create(texture, rect, new Vector2(0.5f, 0.5f), 100.0f);
                    callback(sprite);
                }
            }
        }
    }

    IEnumerator GetTextureRequest_mlbb_frag(string url, System.Action<Sprite> callback)
    {
        using (var www = UnityWebRequestTexture.GetTexture(url))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    var texture = DownloadHandlerTexture.GetContent(www);
                    var rect = new Rect(0, 0, texture.width, texture.height);
                    var sprite = Sprite.Create(texture, rect, new Vector2(0.5f, 0.5f), 100.0f);
                    callback(sprite);
                }
            }
        }
    }
    IEnumerator GetTextureRequest_ff_frag(string url, System.Action<Sprite> callback)
    {
        using (var www = UnityWebRequestTexture.GetTexture(url))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    var texture = DownloadHandlerTexture.GetContent(www);
                    var rect = new Rect(0, 0, texture.width, texture.height);
                    var sprite = Sprite.Create(texture, rect, new Vector2(0.5f, 0.5f), 100.0f);
                    callback(sprite);
                }
            }
        }
    }
    IEnumerator GetTextureRequest_rob_frag(string url, System.Action<Sprite> callback)
    {
        using (var www = UnityWebRequestTexture.GetTexture(url))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    var texture = DownloadHandlerTexture.GetContent(www);
                    var rect = new Rect(0, 0, texture.width, texture.height);
                    var sprite = Sprite.Create(texture, rect, new Vector2(0.5f, 0.5f), 100.0f);
                    callback(sprite);
                }
            }
        }
    }
}
