﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Withdraw_panel_script : MonoBehaviour
{
    public static Withdraw_panel_script Instance;


    public GameObject Diamond_but;

    public GameObject mlbb_panel, freefire_panel, roblox_panel;

    //public Image mlbb_panel_bg, freefire_panel_bg, roblox_panel_bg;

    public Sprite[] diamond_sprite;


    public Image[] BG_img;

    public Image[] mlbb_panel_img;
    public Image[] ff_panel_img;
    public Image[] roblox_panel_img;

    [Header("mlbb")]
    [Space]
    public GameObject mlbb_content;


    [Header("FreeFire")]
    [Space]
    public GameObject freefire_content;


    [Header("Roblox")]
    [Space]
    public GameObject roblox_content;

    public Image[] mlbb, ff, roblox;

    private void Awake()
    {
        Instance = this;
    }

    public void Onclick_panel_change_(GameObject tp)
    {
        mlbb_panel.SetActive(false);
        freefire_panel.SetActive(false);
        roblox_panel.SetActive(false);
        tp.SetActive(true);
    }


    public void generate_but()
    {
        //icon_panel_download();

        foreach (Transform a in mlbb_content.transform)
        {
            Destroy(a.gameObject);
        }

        //MLBB
        for (int i = 0; i < Setting_API.Instance.product_id_withdraw_MLBB.Length; i++)
        {
            //Debug.Log(i);
            GameObject temp = Instantiate(Diamond_but);
            temp.transform.SetParent(mlbb_content.transform);
            temp.transform.localScale = Vector3.one;
            temp.GetComponent<withdraw_but_script>().wth = Setting_API.Instance.coin_type_MLBB[i];
            temp.GetComponent<withdraw_but_script>().Diamond_value.text = Setting_API.Instance.game_dia_withdraw_MLBB[i];
            
            if (temp.GetComponent<withdraw_but_script>().wth == withdraw_type.Diamond)
            {
                temp.GetComponent<withdraw_but_script>().coin_cost = int.Parse(Setting_API.Instance.dia_withdraw_MLBB[i]);
                temp.GetComponent<withdraw_but_script>().cost_value.text = Coin_Manager.instance.Diamond + "/" + Setting_API.Instance.dia_withdraw_MLBB[i];
            }
            else if (temp.GetComponent<withdraw_but_script>().wth == withdraw_type.Goldcoin)
            {
                temp.GetComponent<withdraw_but_script>().coin_cost = int.Parse(Setting_API.Instance.dia_withdraw_MLBB[i]);
                temp.GetComponent<withdraw_but_script>().cost_value.text = Coin_Manager.instance.GoldCoin + "/" + Setting_API.Instance.dia_withdraw_MLBB[i];
            }
            temp.GetComponent<withdraw_but_script>().product_id = Setting_API.Instance.product_id_redeem_MLBB[i];
            temp.GetComponent<withdraw_but_script>().skinImage_link = Setting_API.Instance.coin_img_withdraw_MLBB[i];
            StartCoroutine(GetTextureRequest_mlbb(Firebase_custome_script.Instance.server_link + Setting_API.Instance.coin_img_withdraw_MLBB[i], (response) => {
                temp.GetComponent<withdraw_but_script>().game_diamond.sprite = response;
            }));
            temp.GetComponent<withdraw_but_script>().button_Click();
        }





        foreach (Transform a in freefire_content.transform)
        {
            Destroy(a.gameObject);
        }
        //ff
        for (int i = 0; i < Setting_API.Instance.product_id_withdraw_FF.Length; i++)
        {
            GameObject temp = Instantiate(Diamond_but);
            temp.transform.SetParent(freefire_content.transform);
            temp.transform.localScale = Vector3.one;
            temp.GetComponent<withdraw_but_script>().wth = Setting_API.Instance.coin_type_FF[i];
            //temp.GetComponent<withdraw_but_script>().diamond_image.sprite = diamond_sprite[1];
            temp.GetComponent<withdraw_but_script>().Diamond_value.text = Setting_API.Instance.game_dia_withdraw_FF[i];
            if (temp.GetComponent<withdraw_but_script>().wth == withdraw_type.Diamond)
            {
                temp.GetComponent<withdraw_but_script>().coin_cost = int.Parse(Setting_API.Instance.dia_withdraw_FF[i]);
                temp.GetComponent<withdraw_but_script>().cost_value.text = Coin_Manager.instance.Diamond + "/" + Setting_API.Instance.dia_withdraw_FF[i];
            }
            else if (temp.GetComponent<withdraw_but_script>().wth == withdraw_type.Goldcoin)
            {
                temp.GetComponent<withdraw_but_script>().coin_cost = int.Parse(Setting_API.Instance.dia_withdraw_FF[i]);
                temp.GetComponent<withdraw_but_script>().cost_value.text = Coin_Manager.instance.GoldCoin + "/" + Setting_API.Instance.dia_withdraw_FF[i];
                //temp.GetComponent<withdraw_but_script>().fill_image.fillAmount = (float)Coin_Manager.instance.GoldCoin /float.Parse(Setting_API.Instance.dia_withdraw_MLBB[i]);
            }
            //temp.GetComponent<withdraw_but_script>().cost_value.text = Setting_API.Instance.dia_withdraw_FF[i];
            temp.GetComponent<withdraw_but_script>().product_id = Setting_API.Instance.product_id_withdraw_FF[i];
            temp.GetComponent<withdraw_but_script>().skinImage_link = Setting_API.Instance.coin_img_withdraw_FF[i];
            StartCoroutine(GetTextureRequest_ff(Firebase_custome_script.Instance.server_link + Setting_API.Instance.coin_img_withdraw_FF[i], (response) => {
                temp.GetComponent<withdraw_but_script>().game_diamond.sprite = response;
            }));
            temp.GetComponent<withdraw_but_script>().button_Click();
        }








        foreach (Transform a in roblox_content.transform)
        {
            Destroy(a.gameObject);
        }
        //Roblox
        for (int i = 0; i < Setting_API.Instance.product_id_withdraw_ROB.Length; i++)
        {
            GameObject temp = Instantiate(Diamond_but);
            temp.transform.SetParent(roblox_content.transform);
            temp.transform.localScale = Vector3.one;
            temp.GetComponent<withdraw_but_script>().wth = Setting_API.Instance.coin_type_ROB[i];
            //temp.GetComponent<withdraw_but_script>().diamond_image.sprite = diamond_sprite[0];
            temp.GetComponent<withdraw_but_script>().Diamond_value.text = Setting_API.Instance.game_dia_withdraw_ROB[i];
            if (temp.GetComponent<withdraw_but_script>().wth == withdraw_type.Diamond)
            {
                temp.GetComponent<withdraw_but_script>().coin_cost = int.Parse(Setting_API.Instance.dia_withdraw_ROB[i]);
                temp.GetComponent<withdraw_but_script>().cost_value.text = Coin_Manager.instance.Diamond + "/" + Setting_API.Instance.dia_withdraw_ROB[i];
            }
            else if (temp.GetComponent<withdraw_but_script>().wth == withdraw_type.Goldcoin)
            {
                temp.GetComponent<withdraw_but_script>().coin_cost = int.Parse(Setting_API.Instance.dia_withdraw_ROB[i]);
                temp.GetComponent<withdraw_but_script>().cost_value.text = Coin_Manager.instance.GoldCoin + "/" + Setting_API.Instance.dia_withdraw_ROB[i];
                //temp.GetComponent<withdraw_but_script>().fill_image.fillAmount = (float)Coin_Manager.instance.GoldCoin /float.Parse(Setting_API.Instance.dia_withdraw_MLBB[i]);
            }
            temp.GetComponent<withdraw_but_script>().cost_value.text = Setting_API.Instance.dia_withdraw_ROB[i];
            temp.GetComponent<withdraw_but_script>().product_id = Setting_API.Instance.product_id_redeem_ROB[i];
            temp.GetComponent<withdraw_but_script>().skinImage_link = Setting_API.Instance.coin_img_withdraw_ROB[i];
            StartCoroutine(GetTextureRequest_rob(Firebase_custome_script.Instance.server_link + Setting_API.Instance.coin_img_withdraw_ROB[i], (response) => {
                temp.GetComponent<withdraw_but_script>().game_diamond.sprite = response;
            }));
            temp.GetComponent<withdraw_but_script>().button_Click();
        }




        icon_panel_download();
    }

    private void icon_panel_download()
    {
        int be = Setting_API.Instance.GameName.IndexOf(Setting_API.Instance.type_withdraw_MLBB[0]);
        //Debug.Log()
        StartCoroutine(GetTextureRequest(Firebase_custome_script.Instance.server_link + Setting_API.Instance.icon_panel[be], (response) =>
        {
            mlbb_panel_img[0].sprite = response;
            mlbb_panel_img[1].sprite = response;
            mlbb_panel_img[2].sprite = response;
            //foreach (Image am in mlbb_panel_img)
            //{
            //    am.sprite = response;
            //}
        }));
        StartCoroutine(GetTextureRequest(Firebase_custome_script.Instance.server_link + Setting_API.Instance.Bg_img_panel[be], (response) =>
        {

            BG_img[0].sprite = response;

        }));
        //panel_icon
        int re = Setting_API.Instance.GameName.IndexOf(Setting_API.Instance.type_withdraw_FF[0]);

        StartCoroutine(GetTextureRequest(Firebase_custome_script.Instance.server_link + Setting_API.Instance.icon_panel[re], (response) =>
        {

            ff_panel_img[0].sprite = response;
            ff_panel_img[1].sprite = response;
            ff_panel_img[2].sprite = response;
            //foreach (Image am in ff_panel_img)
            //{
            //    am.sprite = response;
            //}
        }));
        StartCoroutine(GetTextureRequest(Firebase_custome_script.Instance.server_link + Setting_API.Instance.Bg_img_panel[re], (response) =>
        {

            BG_img[2].sprite = response;
           
        }));



        //panel_icon
        

        //panel_icon
        int xe = Setting_API.Instance.GameName.IndexOf(Setting_API.Instance.type_withdraw_ROB[0]);

        StartCoroutine(GetTextureRequest(Firebase_custome_script.Instance.server_link + Setting_API.Instance.icon_panel[xe], (response) =>
        {
            roblox_panel_img[0].sprite = response;
            roblox_panel_img[1].sprite = response;
            roblox_panel_img[2].sprite = response;
            //foreach (Image am in roblox_panel_img)
            //{
            //    am.sprite = response;
            //}
        }));
        StartCoroutine(GetTextureRequest(Firebase_custome_script.Instance.server_link + Setting_API.Instance.Bg_img_panel[xe], (response) =>
        {

            BG_img[1].sprite = response;

        }));

    }


    // Start is called before the first frame update
    void Start()
    {
        //icon_panel_download();
    }

    // Update is called once per frame
    void Update()
    {

    }


    IEnumerator GetTextureRequest(string url, System.Action<Sprite> callback)
    {
        using (var www = UnityWebRequestTexture.GetTexture(url))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    var texture = DownloadHandlerTexture.GetContent(www);
                    var rect = new Rect(0, 0, texture.width, texture.height);
                    var sprite = Sprite.Create(texture, rect, new Vector2(0.5f, 0.5f), 100.0f);
                    callback(sprite);
                }
            }
        }
    }


    IEnumerator GetTextureRequest_mlbb(string url, System.Action<Sprite> callback)
    {
        using (var www = UnityWebRequestTexture.GetTexture(url))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    var texture = DownloadHandlerTexture.GetContent(www);
                    var rect = new Rect(0, 0, texture.width, texture.height);
                    var sprite = Sprite.Create(texture, rect, new Vector2(0.5f, 0.5f), 100.0f);
                    callback(sprite);
                }
            }
        }
    }
    IEnumerator GetTextureRequest_ff(string url, System.Action<Sprite> callback)
    {
        using (var www = UnityWebRequestTexture.GetTexture(url))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    var texture = DownloadHandlerTexture.GetContent(www);
                    var rect = new Rect(0, 0, texture.width, texture.height);
                    var sprite = Sprite.Create(texture, rect, new Vector2(0.5f, 0.5f), 100.0f);
                    callback(sprite);
                }
            }
        }
    }
    IEnumerator GetTextureRequest_rob(string url, System.Action<Sprite> callback)
    {
        using (var www = UnityWebRequestTexture.GetTexture(url))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    var texture = DownloadHandlerTexture.GetContent(www);
                    var rect = new Rect(0, 0, texture.width, texture.height);
                    var sprite = Sprite.Create(texture, rect, new Vector2(0.5f, 0.5f), 100.0f);
                    callback(sprite);
                }
            }
        }
    }

}
