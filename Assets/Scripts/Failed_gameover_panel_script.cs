using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Failed_gameover_panel_script : MonoBehaviour
{
    public GameObject cube,rotation_parent,Nothanks_but;




    private void OnEnable()
    {

        cube.GetComponent<cube_script>().cube_typpe = PlayerPrefs.GetInt("Max_num",8).ToString();
        rotation_parent.SetActive(true);
        StartCoroutine("wait_for_nothanks");
    }


    IEnumerator wait_for_nothanks()
    {
        yield return new WaitForSecondsRealtime(2f);
        Nothanks_but.SetActive(true);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        rotation_parent.transform.Rotate(0, 2.0f * 50f * Time.deltaTime, 0);
        //cube.transform.ro
    }


    public void On_Continue()
    {
        Ads_priority_script.Instance.watch_video(Ads_reward_type.GameoverPre);
    }

    private void OnDisable()
    {
        rotation_parent.SetActive(false);
    }

    public void On_Nothanks()
    {
        Controller_script.instance.REstart_game();
    }
}
