using DG.Tweening;
using UnityEngine;

public class New_cube_panel : MonoBehaviour
{

    public GameObject cube;
    //public GameObject claim_but;

    // Start is called before the first frame update
    void Start()
    {
        //claim_but.GetComponent<Button>().onClick.AddListener(() => onclaim());
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnEnable()
    {
        cube.transform.localEulerAngles = new Vector3(3.419f, 0f, -59.37f);
        //cube.transform.DOLocalRotateQuaternion(Quaternion.Euler(3.419f, 179f, -59.37f), 3f).OnComplete(() => { gameObject.SetActive(false); GameManager.Instance.start_time(); Slider_cube_scripts.instance.move_cube_inv(); }).SetUpdate(true);
        if(Random.value <0.5)
        {
            cube.transform.DOLocalRotateQuaternion(Quaternion.Euler(3.419f, 179f, -59.37f), 3f).OnComplete(() => { gameObject.SetActive(false); Canvas_singleton_script.Instance.spin_panel.GetComponent<FortuneWheelManager>().ct = coin_type.Gold;  Canvas_singleton_script.Instance.spin_panel.SetActive(true); }).SetUpdate(true);
        }
        else
        {
            cube.transform.DOLocalRotateQuaternion(Quaternion.Euler(3.419f, 179f, -59.37f), 3f).OnComplete(() => { gameObject.SetActive(false); Canvas_singleton_script.Instance.spin_panel.GetComponent<FortuneWheelManager>().ct = coin_type.Diamond; Canvas_singleton_script.Instance.spin_panel.SetActive(true); }).SetUpdate(true);
        }

    }
    public void onclaim()
    {

    }

}
