﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class fragment_panel_script : MonoBehaviour
{
    //protected override void OnApplicationQuitCallback()
    //{
    //    //throw new System.NotImplementedException();
    //}

    //protected override void OnEnableCallback()
    //{
    //    //throw new System.NotImplementedException();
    //}

public static fragment_panel_script Instance;


    //public Image skin;
    public int skin_num;
    public string skin_name;


    public GameObject skin_icon_obj;

    public Text skin_name_text;

    public GameObject sun_shine;
    public GameObject sparks;
    public GameObject frag;
    public GameObject Claim_but;
    public GameObject nothanks_but;

    public bool skin_panel = false;






    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnEnable()
    {
        skin_name_text.gameObject.SetActive(false);
        sun_shine.SetActive(false);
        sparks.SetActive(false);
        frag.SetActive(false);
        skin_icon_obj.transform.localScale = new Vector3(100,100,100);
        skin_icon_obj.GetComponent<MeshRenderer>().material.SetFloat("_SliceAmount", 1f);
        skin_icon_obj.GetComponent<MeshRenderer>().material.DOFloat( 0f, "_SliceAmount", 3f).OnComplete(()=> skin_icon_obj.transform.DOScale(new Vector3(50, 50, 50), 1f).SetUpdate(true).OnComplete(() => turn_on_claim_but())).SetUpdate(true);

    }


    public void turn_on_claim_but()
    {
        skin_name_text.gameObject.SetActive(true);
        sun_shine.SetActive(true);
        sparks.SetActive(true);
        frag.SetActive(true);
        Claim_but.SetActive(true);

    }



    public void Claim_but_click()
    {
        Ads_priority_script.Instance.frag_watch_video(skin_name);
        //Canvas_singleton_script.Instance.fragment_claim();
    }

    public void nothanks_but_click()
    {
        if(skin_panel)
        {
            Canvas_singleton_script.Instance.Redeem_panel.SetActive(true);
            skin_panel = false;
        }
        gameObject.SetActive(false);
    }
}
