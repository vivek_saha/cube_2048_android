using System.Collections.Generic;
using UnityEngine;

public class Game_selection_panel : MonoBehaviour
{
    public static Game_selection_panel instance;

    public List<GameObject> GameList = new List<GameObject>();

    public GameObject[] Filled_bar;


    public List<GameObject> panel_list = new List<GameObject>();

    public GameObject game_selection_but_prefab;
    public Transform content;



    public int Counter = 0;



    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    public void OnSubmit()
    {
        if (Counter == 3)
        {
            PlayerPrefs.SetString("Game0", GameList[0].GetComponent<Game_selection_but_script>().game_name);
            PlayerPrefs.SetString("Game1", GameList[1].GetComponent<Game_selection_but_script>().game_name);
            PlayerPrefs.SetString("Game2", GameList[2].GetComponent<Game_selection_but_script>().game_name);
            this.gameObject.SetActive(false);
            Setting_API.Instance.Generate_redeem_diamond_panel();
            GameManager.Instance.game_start = true;
        }
        else
        {
            GameManager.Instance.nem_popup_fun("!!!Please select 3 Games!!!");
        }
    }


    public void Generate_game_list()
    {
        if (content.transform.childCount != 0)
        {
            foreach (Transform a in content.transform)
            {
                Destroy(a.gameObject);
            }
        }
        panel_list.Clear();
        for (int i = 0; i < Setting_API.Instance.GameName.Count; i++)
        {
            GameObject temp = Instantiate(game_selection_but_prefab);
            temp.transform.SetParent(content.transform);
            temp.transform.localScale = Vector3.one;
            temp.transform.localPosition = Vector3.zero;
            temp.transform.localRotation = Quaternion.identity;

            temp.GetComponent<Game_selection_but_script>().game_name = Setting_API.Instance.GameName[i];
            temp.GetComponent<Game_selection_but_script>().game_name_text.text = Setting_API.Instance.GameName[i].ToString();
            temp.GetComponent<Game_selection_but_script>().game_icon_link = Setting_API.Instance.icon_start_panel[i];
            temp.GetComponent<Game_selection_but_script>().add_listner();
            panel_list.Add(temp);
        }
    }



    public void selection_but_Counter(Game_selection_but_script a)
    {
        if (Counter < 3)
        {
            if (a.Selected.activeSelf)
            {
                a.Deselected_fun();
                GameList.Remove(a.gameObject);
                Counter--;

                Filled_bar[Counter].SetActive(false);

            }
            else
            {
                a.Selected_fun();
                GameList.Add(a.gameObject);
                Counter++;
                Filled_bar[Counter - 1].SetActive(true);
            }
        }
        else
        {
            if (a.Selected.activeSelf)
            {
                a.Deselected_fun();
                GameList.Remove(a.gameObject);
                Counter--;

                Filled_bar[Counter].SetActive(false);

            }
            else
            {

                GameList[GameList.Count - 1].GetComponent<Game_selection_but_script>().Deselected_fun();
                GameList.Remove(GameList[GameList.Count - 1]);
                a.Selected_fun();
                GameList.Add(a.gameObject);
            }
        }
    }

    public void Preselect_fun()
    {
        if (PlayerPrefs.GetString("Game0", string.Empty) != string.Empty)
        {
            selection_but_Counter(panel_list.Find(x => x.GetComponent<Game_selection_but_script>().game_name == PlayerPrefs.GetString("Game0")).GetComponent<Game_selection_but_script>());
        }
        if (PlayerPrefs.GetString("Game1", string.Empty) != string.Empty)
        {
            selection_but_Counter(panel_list.Find(x => x.GetComponent<Game_selection_but_script>().game_name == PlayerPrefs.GetString("Game1")).GetComponent<Game_selection_but_script>());
        }
        if (PlayerPrefs.GetString("Game2", string.Empty) != string.Empty)
        {
            selection_but_Counter(panel_list.Find(x => x.GetComponent<Game_selection_but_script>().game_name == PlayerPrefs.GetString("Game2")).GetComponent<Game_selection_but_script>());
        }
    }

}
