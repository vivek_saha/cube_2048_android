﻿using UnityEngine;

public enum coin_type { Diamond, Gold }
public class Canvas_singleton_script : MonoBehaviour
{


    public static Canvas_singleton_script Instance;


    [Header("Panels")]
    public GameObject Withdraw_panel;
    public GameObject Redeem_panel;
    //public GameObject Top_panel;
    public GameObject Contact_us_panel;
    public GameObject fragment_panel;
    public GameObject Popup_withdraw_panel;
    public GameObject NewCube_panel;
    public GameObject spin_panel;
    public GameObject origin;
    public GameObject failed_gameover_panel;
    public GameObject GameSelection_panel;

    public withdraw_type wth;
    //public int cost;

    private void Awake()
    {
        Instance = this;
    }


    private void Start()
    {
        //Withdraw_panel.GetComponent<Withdraw_panel_script>().generate_but();
        //Redeem_panel.GetComponent<Redeem_panel_script>().generate_but();
    }

    //protected override void OnApplicationQuitCallback()
    //{

    //}

    //protected override void OnEnableCallback()
    //{

    //}

    public void onwithdraw_close()
    {
        Withdraw_panel.SetActive(false);
    }
    public void onredeem_close()
    {
        Redeem_panel.SetActive(false);
    }

    public void Close_click()
    {
        if (Redeem_panel.activeSelf)
        {
            onredeem_close();
        }
        if (Withdraw_panel.activeSelf)
        {
            onwithdraw_close();
        }
        //Top_panel.SetActive(false);
    }


    public void On_click_contact_us()
    {
        Contact_us_panel.SetActive(true);
    }

    public void fragment_claim()
    {
        if (fragment_panel_script.Instance.skin_panel)
        {
            Redeem_panel.gameObject.SetActive(true);
            fragment_panel_script.Instance.skin_panel = false;
        }
        int a = PlayerPrefs.GetInt(fragment_panel_script.Instance.skin_name);
        PlayerPrefs.SetInt(fragment_panel_script.Instance.skin_name, a + 3);
        fragment_panel.SetActive(false);
        GameManager.Instance.start_time();
    }

    public void FrageMent_skin_open()
    {
        if (!fragment_panel.activeSelf)
        {
            GameManager.Instance.stop_time();
            int sel = Random.Range(0, 3);
            int a;
            string name_s;
            switch (sel)
            {
                case 0:
                    a = Random.Range(0, Setting_API.Instance.product_id_redeem_MLBB.Length);
                    Debug.Log("this frag" + a);
                    name_s = Setting_API.Instance.product_id_redeem_MLBB[a];
                    fragment_panel.GetComponent<fragment_panel_script>().skin_name_text.text = Setting_API.Instance.name_redeem_MLBB[a];
                    fragment_panel.GetComponent<fragment_panel_script>().skin_name = name_s;
                    fragment_panel.GetComponent<fragment_panel_script>().skin_icon_obj.GetComponent<MeshRenderer>().materials[0].mainTexture = Redeem_panel.GetComponent<Redeem_panel_script>().mlbb_content.transform.GetChild(a).GetComponent<skin_redeem_but_script>().Fragement_sprite.texture;
                    break;
                case 1:
                    a = Random.Range(0, Setting_API.Instance.product_id_redeem_FF.Length);
                    Debug.Log("this frag" + a);
                    name_s = Setting_API.Instance.product_id_redeem_FF[a];
                    fragment_panel.GetComponent<fragment_panel_script>().skin_name_text.text = Setting_API.Instance.name_redeem_FF[a];
                    fragment_panel.GetComponent<fragment_panel_script>().skin_name = name_s;
                    fragment_panel.GetComponent<fragment_panel_script>().skin_icon_obj.GetComponent<MeshRenderer>().materials[0].mainTexture = Redeem_panel.GetComponent<Redeem_panel_script>().freefire_content.transform.GetChild(a).GetComponent<skin_redeem_but_script>().Fragement_sprite.texture;
                    break;
                case 2:
                    a = Random.Range(0, Setting_API.Instance.product_id_redeem_ROB.Length);
                    Debug.Log("this frag" + a);
                    name_s = Setting_API.Instance.product_id_redeem_ROB[a];
                    fragment_panel.GetComponent<fragment_panel_script>().skin_name_text.text = Setting_API.Instance.name_redeem_ROB[a];
                    fragment_panel.GetComponent<fragment_panel_script>().skin_name = name_s;
                    fragment_panel.GetComponent<fragment_panel_script>().skin_icon_obj.GetComponent<MeshRenderer>().materials[0].mainTexture = Redeem_panel.GetComponent<Redeem_panel_script>().roblox_content.transform.GetChild(a).GetComponent<skin_redeem_but_script>().Fragement_sprite.texture;
                    break;
            }
            fragment_panel.SetActive(true);
        }
    }

    public void FrageMent_skin_open(string s_name, Sprite s_image, string skin_name_text)
    {
        Redeem_panel_script.Instance.gameObject.SetActive(false);
        fragment_panel.GetComponent<fragment_panel_script>().skin_panel = true;
        fragment_panel.GetComponent<fragment_panel_script>().skin_name = s_name;
        fragment_panel.GetComponent<fragment_panel_script>().skin_name_text.text = skin_name_text;
        fragment_panel.GetComponent<fragment_panel_script>().skin_icon_obj.GetComponent<MeshRenderer>().materials[0].mainTexture = s_image.texture;
        fragment_panel.SetActive(true);
    }

    public void contact_us()
    {
        //AnimationManager.instance.Contact_mail();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Close_click();
        }
    }

    private void OnEnable()
    {
        Controller_script.newcubes_ += Turn_on_newcube_panel;

    }


    private void OnDisable()
    {
        Controller_script.newcubes_ -= Turn_on_newcube_panel;
    }

    public void Turn_on_newcube_panel(int num)
    {
        AudioManager.Instance.new_cube_created_play();
        NewCube_panel.GetComponent<New_cube_panel>().cube.GetComponent<cube_script>().cube_typpe = PlayerPrefs.GetInt("Max_num", 8).ToString();
        NewCube_panel.GetComponent<New_cube_panel>().cube.GetComponent<MeshRenderer>().material.mainTexture = NewCube_panel.GetComponent<New_cube_panel>().cube.GetComponent<cube_script>().color_list[(int)Mathf.Log(num, 2)];
        NewCube_panel.SetActive(true);

    }


}

