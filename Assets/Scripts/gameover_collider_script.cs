using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameover_collider_script : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<cube_script>() != null && other.GetComponent<cube_script>().init_cube == false && other.GetComponent<cube_script>().hit_bool == true)
        {
            //failed_popup
            Debug.Log("failed");
            if(!Canvas_singleton_script.Instance.failed_gameover_panel.gameObject.activeSelf)
            {
                GameManager.Instance.stop_time();
                AudioManager.Instance.fail_play();
                Canvas_singleton_script.Instance.failed_gameover_panel.SetActive(true);
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<cube_script>() != null && other.GetComponent<cube_script>().init_cube == false && other.GetComponent<cube_script>().hit_bool == true)
        {
            //failed_popup
            Debug.Log("failed");
            if (!Canvas_singleton_script.Instance.failed_gameover_panel.gameObject.activeSelf)
            {
                GameManager.Instance.stop_time();
                AudioManager.Instance.fail_play();
                Canvas_singleton_script.Instance.failed_gameover_panel.SetActive(true);
            }
        }
    }
}
