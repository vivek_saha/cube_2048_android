﻿using SimpleJSON;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;


public enum Message_type { Admin, User }
public class ContactUS_API : MonoBehaviour
{

    public static ContactUS_API Instance;
    public string Message;
    string rawJson;
    public JSONNode jsonResult;
    public int is_message = 0;


    private void Awake()
    {
        Instance = this;
    }



    public void Update_data()
    {
        GetComponent<Contact_us_panel_Script>().Loading_panel.SetActive(true);
        StartCoroutine("Upload");

    }


    IEnumerator Upload()
    {
        WWWForm form = new WWWForm();
        //form.AddField("userid", PlayerPrefs.GetString("user_id", ""));
        form.AddField("massage", Message);
        form.AddField("is_message", is_message);
        //if (is_update == 1)
        //{
        //    form.AddField("type", type);
        //    if (type == "purchase")
        //    {
        //        form.AddField("order_id", order_id);
        //    }
        //    form.AddField("wallet_coin", credits.ToString());
        //    form.AddField("current_xp", xp);
        //}
        //if (is_profile == 1)
        //{
        //    form.AddField("username", username);
        //    form.AddBinaryData("profile_picture", edit_profile_panel.I.itemBGBytes, "temp.png", "image/png");
        //}
        UnityWebRequest www = UnityWebRequest.Post(Firebase_custome_script.Instance.server_link + "/api/contact", form);
        www.SetRequestHeader("Authorization", PlayerPrefs.GetString("Token"));
        yield return www.SendWebRequest();
        Debug.Log("www: " + www.responseCode);
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            //Check_Internet_connection();
        }
        else
        {
            //close_Retry_panel();

            rawJson = www.downloadHandler.text;
            Debug.Log("ContactUs_API_data upload complete!" + rawJson);
            is_message = 0;
            //GetComponent<LeaderBoard_API>().start_lederboard_api();
            //Get_response_data();
            SEt_updated_profile();
        }

    }



    public void SEt_updated_profile()
    {
        PlayerPrefs.SetInt("1st_time", 1);
        if (GetComponent<Contact_us_panel_Script>().content.transform.childCount > 0)
        {
            for (int i = 0; i < GetComponent<Contact_us_panel_Script>().content.transform.childCount; i++)
            {
                Destroy(GetComponent<Contact_us_panel_Script>().content.transform.GetChild(i).gameObject);
            }

        }
        GameObject temp;
        jsonResult = JSON.Parse(rawJson);
        foreach (JSONNode data in jsonResult["data"])
        {

            if (data["is_reply"].Value == "1")
            {

                temp = null;

                temp = Instantiate(GetComponent<Contact_us_panel_Script>().Admin_msg_container, Vector3.zero, Quaternion.identity);
                temp.transform.parent = GetComponent<Contact_us_panel_Script>().content.transform;
                temp.transform.localScale = new Vector3(1, 1, 1);
                temp.GetComponent<Msg_container_script>().Msg.text = data["replymessage"].Value;



            }
            else
            {
                temp = Instantiate(GetComponent<Contact_us_panel_Script>().User_msg_container, Vector3.zero, Quaternion.identity);
                temp.transform.parent = GetComponent<Contact_us_panel_Script>().content.transform;
                temp.transform.localScale = new Vector3(1, 1, 1);
                temp.GetComponent<Msg_container_script>().Msg.text = data["message"].Value;
            }
            temp = null;
        }
        //if(Message_type.Admin)
        //{

        //}
        GetComponent<Contact_us_panel_Script>().Loading_panel.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
