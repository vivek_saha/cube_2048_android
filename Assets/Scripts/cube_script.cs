using System.Collections;
using UnityEngine;

public class cube_script : MonoBehaviour
{
    public Texture[] color_list;
    public delegate void hit();
    public delegate void Mergehit(GameObject a, GameObject b);
    public static event hit Collide_hit;
    public static event Mergehit Merge_hit;
    public string cube_typpe;
    public bool hit_bool = false;
    public bool merge = true;
    public bool init_cube = true;
    public bool tower_cube = false;
    public bool explode = false;
    public bool rotate_obj = false;
    public GameObject explosionParticle;
    float new11 = 100f;

    public bool new_cube_panel = false;

    float jumpHeight = 5f;
    public GameObject target;

    // Start is called before the first frame update
    void Start()
    {
        if (tower_cube)
        {
            GetComponent<MeshRenderer>().material.mainTexture = color_list[(int)Mathf.Log(float.Parse(cube_typpe), 2) - 1];
            Controller_script.instance.List_of_cubes.Add(this.gameObject);
        }
        StartCoroutine("enable_merge");
    }

    IEnumerator enable_merge()
    {
        yield return new WaitForSeconds(0.2f);
        merge = false;
    }

    private void OnEnable()
    {
        if (new_cube_panel)
        {
            GetComponent<MeshRenderer>().material.mainTexture = color_list[(int)Mathf.Log(float.Parse(cube_typpe), 2) - 1];
            //Controller_script.instance.List_of_cubes.Add(this.gameObject);
        }
    }


    // Update is called once per frame
    void FixedUpdate()
    {
    }

    private void Update()
    {
        if (this.rotate_obj)
        {
            this.transform.Rotate(6.0f * new11 * Time.deltaTime, 0, 0);
        }

        if (explode)
        {
            if (this.gameObject != null && target!= null)
            {
                //Debug.Log(target.transform.position);
                Vector3 mk = calcBallisticVelocityVector(gameObject.transform.position, target.transform.position, 80);
                //if(mk == nan)
                if (!float.IsNaN(mk.x) && !float.IsNaN(mk.y) && !float.IsNaN(mk.z))
                {
                    //Do stuff
                    GetComponent<Rigidbody>().AddForce(mk, ForceMode.Impulse);
                }
                explode = false;
                this.rotate_obj = true;
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        this.rotate_obj = false;
        if(collision.gameObject.tag == "Fragment" && !this.hit_bool )
        {
            //this.hit_bool = true;
            //this.init_cube = false;
            //Controller_script.instance.List_of_cubes.Add(this.gameObject);
            //Collide_hit();
            Destroy(collision.gameObject);
            Canvas_singleton_script.Instance.FrageMent_skin_open();
        }
        if (collision.gameObject.name != "path" && collision.gameObject.tag != "Fragment" && !this.hit_bool)
        {
            this.hit_bool = true;
            this.init_cube = false;
            Controller_script.instance.List_of_cubes.Add(this.gameObject);
            Collide_hit();
        }

        if (!this.merge && collision.gameObject.name != "path" && collision.gameObject.GetComponent<cube_script>() != null && this.hit_bool && !collision.gameObject.GetComponent<cube_script>().merge)
        {
            if (collision.gameObject.GetComponent<cube_script>().cube_typpe == this.cube_typpe && Controller_script.instance.List_of_cubes.Contains(this.gameObject))
            {
                collision.gameObject.GetComponent<BoxCollider>().enabled = false;
                this.merge = true;
                Merge_hit(this.gameObject, collision.gameObject);
            }
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        this.rotate_obj = false;
        if (collision.gameObject.tag == "Fragment" && !this.hit_bool)
        {
            //this.hit_bool = true;
            //this.init_cube = false;
            //Controller_script.instance.List_of_cubes.Add(this.gameObject);
            //Collide_hit();
            Destroy(collision.gameObject);
            Canvas_singleton_script.Instance.FrageMent_skin_open();
        }
        if (collision.gameObject.name != "path" && collision.gameObject.tag != "Fragment" && !this.hit_bool)
        {
            this.hit_bool = true;
            this.init_cube = false;
            Controller_script.instance.List_of_cubes.Add(this.gameObject);
            Collide_hit();
        }

        if (!this.merge && collision.gameObject.name != "path" && collision.gameObject.GetComponent<cube_script>() != null && this.hit_bool && !collision.gameObject.GetComponent<cube_script>().merge)
        {
            if (collision.gameObject.GetComponent<cube_script>().cube_typpe == this.cube_typpe && Controller_script.instance.List_of_cubes.Contains(this.gameObject))
            {
                collision.gameObject.GetComponent<BoxCollider>().enabled = false;
                this.merge = true;
                Merge_hit(this.gameObject, collision.gameObject);
            }
        }
    }

    void JumpTowardPoint()
    {
        float gravity = Physics.gravity.magnitude;
        float initialVelocity = CalculateJumpSpeed(jumpHeight, gravity);

        Vector3 direction = target.transform.position - transform.position;
        Debug.Log("initialVelocity" + initialVelocity);
        Debug.Log("direction" + direction);
        GetComponent<Rigidbody>().AddRelativeForce(initialVelocity * direction, ForceMode.Impulse);
    }

    private float CalculateJumpSpeed(float jumpHeight, float gravity)
    {
        return Mathf.Sqrt(2 * jumpHeight * gravity);
    }
    Vector3 calcBallisticVelocityVector(Vector3 source, Vector3 target, float angle)
    {
        Vector3 direction = target - source;
        float h = direction.y;
        direction.y = 0;
        float distance = direction.magnitude;
        float a = angle * Mathf.Deg2Rad;
        direction.y = distance * Mathf.Tan(a);
        distance += h / Mathf.Tan(a);

        // calculate velocity
        float velocity = Mathf.Sqrt(distance * Physics.gravity.magnitude / Mathf.Sin(2 * a));
        return velocity * direction.normalized;
    }   
}
