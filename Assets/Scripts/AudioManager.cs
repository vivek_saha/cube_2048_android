﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    public static AudioManager Instance;

    public AudioSource  Button_click,bg_music;


    public AudioSource event_audiosource, spin_source,merge_cube_source; 
    //public GameObject coin_fall_prefab;
    public AudioClip Fragment_win_clip, lose_clip, New_cube_created_clip, spin_sound_clip, merge_cube_clip;


    public bool Music_bool
    {
        get
        {
            if (PlayerPrefs.GetString("Music_bool", "True") == "True")
            {
             
                return true;
            }
            else
            {
             
                return false;
            }
        }
        set
        {
            
            if (value)
            {
                bg_music.enabled = true;
                //eneble_sounds();
                PlayerPrefs.SetString("Music_bool", value.ToString());
            }
            else
            {
                bg_music.enabled = false;
                //disable_sounds();
                PlayerPrefs.SetString("Music_bool", value.ToString());
            }
            
        }
    }

    public bool Sound_bool
    {
        get
        {
            if (PlayerPrefs.GetString("Sound_bool", "True") == "True")
            {
                //eneble_sounds();
                return true;
            }
            else
            {
                //disable_sounds();
                return false;
            }
        }
        set
        {
            //Debug.Log(value);
            if (value)
            {
                eneble_sounds();
                PlayerPrefs.SetString("Sound_bool", value.ToString());
            }
            else
            {
                disable_sounds();
                PlayerPrefs.SetString("Sound_bool", value.ToString());
            }
            //Setting_panel_script.Instance.set_toogles();
        }
    }

    public bool Vibrate_bool
    {
        get
        {
            if (PlayerPrefs.GetString("Vibrate_bool", "True") == "True")
            {
                //eneble_sounds();
                return true;
            }
            else
            {
                //disable_sounds();
                return false;
            }
        }
        set
        {
            if (value)
            {
                //eneble_sounds();
                PlayerPrefs.SetString("Vibrate_bool", value.ToString());
            }
            else
            {
                //disable_sounds();
                PlayerPrefs.SetString("Vibrate_bool", value.ToString());
            }
            //Setting_panel_script.Instance.set_toogles();
        }
    }
    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {

        Sound_bool = Sound_bool;
        Vibrate_bool = Vibrate_bool;
        GameManager.Instance.set_script.set_toogles();
        Debug.Log("sounds:" + Sound_bool);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void eneble_sounds()
    {
        //Coin_throw_sound.enabled = true;
        //Monster_hit_sound.enabled = true;
        merge_cube_source.enabled = true;
        spin_source.enabled = true;
        event_audiosource.enabled = true;
        Button_click.enabled = true;
    }


    public void disable_sounds()
    {
        //Coin_throw_sound.enabled = false;
        //Monster_hit_sound.enabled = false;
        merge_cube_source.enabled = false;
        spin_source.enabled = false;
        event_audiosource.enabled = false;
        Button_click.enabled = false;
    }


    public void Button_click_sound()
    {
        Button_click.Play();
    }

    //public void Coin_fall_sounds()
    //{
    //    //Instantiate(coin_fall_prefab);
    //}

    public void Lose_sound_play()
    {
        event_audiosource.clip = lose_clip;
        event_audiosource.Play();
    }

    public void Merge_cube_play()
    {
        merge_cube_source.clip = merge_cube_clip;
        merge_cube_source.Play();
    }


    public void Fragment_win_play()
    {
        event_audiosource.clip = Fragment_win_clip;
        event_audiosource.Play();
    }

    public void new_cube_created_play()
    {
        event_audiosource.clip = New_cube_created_clip;
        event_audiosource.Play();
    }

    public void fail_play()
    {
        event_audiosource.clip = lose_clip;
        event_audiosource.Play();
    }

    public void Spin_start()
    {
        spin_source.clip = spin_sound_clip;
        spin_source.loop = true;
        //spin_source.pitch = 0.1f;
        spin_source.Play();
    }
    public void Spin_stop()
    {
        spin_source.Stop();
    }

    //private static readonly AndroidJavaObject Vibrator =
    //new AndroidJavaClass("com.unity3d.player.UnityPlayer")// Get the Unity Player.
    //.GetStatic<AndroidJavaObject>("currentActivity")// Get the Current Activity from the Unity Player.
    //.Call<AndroidJavaObject>("getSystemService", "vibrator");// Then get the Vibration Service from the Current Activity.

    //static void KyVibrator()
    //{
    //    // Trick Unity into giving the App vibration permission when it builds.
    //    // This check will always be false, but the compiler doesn't know that.
    //    if (Application.isEditor) Handheld.Vibrate();
    //}

    //public static void Vibrate(long milliseconds)
    //{
    //    Vibrator.Call("vibrate", milliseconds);
    //}

    //public static void Vibrate(long[] pattern, int repeat)
    //{
    //    Vibrator.Call("vibrate", pattern, repeat);
    //}
}
