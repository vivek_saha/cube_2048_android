using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Contact_us_panel_bg_script : MonoBehaviour
{


    public bool change_heights = false;
    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log(Screen.height/Screen.width);
        GetComponent<RectTransform>().sizeDelta = new Vector2((608.619f*((float)Screen.height/ (float)Screen.width))- 1.67266f, GetComponent<RectTransform>().rect.height);


        GetComponent<RectTransform>().offsetMin = new Vector2(GetComponent<RectTransform>().offsetMin.x, 0);
        GetComponent<RectTransform>().offsetMax = new Vector2(GetComponent<RectTransform>().offsetMax.x, 0);
        if(change_heights)
        {

            GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().rect.width, Screen.height);
        }
    }

    // Update is called once per frame
    void Update()
    {
    }
}
