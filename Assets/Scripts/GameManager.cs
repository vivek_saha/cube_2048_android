﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{

    public GameObject Canvas_singleton;

    //public GameObject speen;
    public Setting_panel_script set_script;
    int ads_counter = 0;

    public GameObject popup_message;

    public GameObject Merge_particle;
    public GameObject particle_camera;
    public GameObject fragment_bubble;

    public bool game_start = false;

    protected override void OnApplicationQuitCallback()
    {
        
    }

    protected override void OnEnableCallback()
    {
       
    }



    public void nem_popup_fun(string msg)
    {
        //GameObject a = Instantiate(popup_message);
        //a.GetComponent<nem_popup_script>().msg.text = msg;
        //a.transform.SetParent(this.transform);
        //a.transform.localScale = Vector3.one;
        //a.transform.position = Canvas_singleton_script.Instance.origin.transform.position;


        _ShowAndroidToastMessage(msg);
    }

    private void _ShowAndroidToastMessage(string message)
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        if (unityActivity != null)
        {
            AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
            AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity, message, 0);
            toastObject.Call("show");
            //unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
            //{
            //}));
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.GetInt("First_time", 0) != 0)
        {
            SaveSystem_s.Loadgame();
        }
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_EDITOR
        if(Input.GetKeyDown(KeyCode.S))
        {
            SaveSystem_s.Savegame();
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            SaveSystem_s.Loadgame();
        }
#endif
    }

    
    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            SaveSystem_s.Savegame();
            Update_API.Instance.Update_data();
        }
    }


    public void stop_time()
    {
        particle_camera.SetActive(false);
        if (ads_counter >= Ads_API.Instance.ads_click)
        {
            Ads_priority_script.Instance.Show_interrestial();
            ads_counter = 0;
        }
        else
        {
            ads_counter++;
        }
        Time.timeScale = 0;
    }

    public void stop_time_no_ads()
    {
        particle_camera.SetActive(false);
        Time.timeScale = 0;
    }

    public void stop_time_and_show_interstatial()
    {
        particle_camera.SetActive(false);
        Ads_priority_script.Instance.Show_interrestial();
        Time.timeScale = 0;
    }
    public void start_time()
    {
        particle_camera.SetActive(true);
        Time.timeScale = 1;
    }
}

public static class AbbrevationUtility
{
    private static readonly SortedDictionary<long, string> abbrevations = new SortedDictionary<long, string>
     {
         {1000,"K"},
         {1000000, "M" },
         {1000000000, "B" },
         {1000000000000,"T"}
     };

    public static string AbbreviateNumber(float number)
    {
        for (int i = abbrevations.Count - 1; i >= 0; i--)
        {
            KeyValuePair<long, string> pair = abbrevations.ElementAt(i);
            if (Mathf.Abs(number) >= pair.Key)
            {
                float roundedNumber = (number / pair.Key);
                roundedNumber = (float)System.Math.Round(roundedNumber, 2);
                return roundedNumber.ToString() + pair.Value;
            }
        }
        return number.ToString();
    }
}
public static class AbbrevationUtility_LeaderBoard
{
    private static readonly SortedDictionary<long, string> abbrevations = new SortedDictionary<long, string>
     {
         {1000,"K"},
         {1000000, "M" },
         {1000000000, "B" },
         {1000000000000,"T"}
     };

    public static string AbbreviateNumber(float number)
    {
        for (int i = abbrevations.Count - 1; i >= 0; i--)
        {
            KeyValuePair<long, string> pair = abbrevations.ElementAt(i);
            if (Mathf.Abs(number) >= pair.Key)
            {
                float roundedNumber = (number / pair.Key);
                roundedNumber = (float)System.Math.Round(roundedNumber, 2);
                return roundedNumber.ToString() + pair.Value;
            }
        }
        return number.ToString();
    }
}
