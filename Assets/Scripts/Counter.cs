using UnityEngine;
using UnityEngine.UI;

public class Counter : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.name == "Gold_coin_value")
        {
            GetComponent<Text>().text = AbbrevationUtility.AbbreviateNumber(Coin_Manager.instance.GoldCoin);
        }
        else if (gameObject.name == "Diamond_value")
        {
            GetComponent<Text>().text = (Mathf.Round(Coin_Manager.instance.Diamond * 10.0f) * 0.1f).ToString();
        }
        else if (gameObject.name == "Fill_bar")
        {
            //GetComponent<Image>().fillAmount;

        }
    }
}
