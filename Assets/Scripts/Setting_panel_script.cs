﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Setting_panel_script : MonoBehaviour
{
    public static Setting_panel_script Instance;
    //public Setting_api_script set_api;
    public Toggle sound, vibrate,music;


    public Sprite sound_on,sound_off,music_on,music_off, vibrate_on, vibrate_off;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void set_toogles()
    {
        music.isOn = AudioManager.Instance.Music_bool;
        if (music.isOn)
        {
            music.GetComponent<Image>().sprite = music_on;
        }
        else
        {
            music.GetComponent<Image>().sprite = music_off;
        }
        sound.isOn = AudioManager.Instance.Sound_bool;
        if(sound.isOn)
        {
            sound.GetComponent<Image>().sprite = sound_on;
        }
        else
        {
            sound.GetComponent<Image>().sprite = sound_off;
        }
        //Debug.Log(sound.isOn);
        vibrate.isOn = AudioManager.Instance.Vibrate_bool;
        if (vibrate.isOn)
        {
            vibrate.GetComponent<Image>().sprite = vibrate_on;
        }
        else
        {
            vibrate.GetComponent<Image>().sprite = vibrate_off;
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }


    public void Onstart()
    {
        transform.DOLocalMove(Vector3.zero, 0.5f).SetUpdate(true);
    }
    public void OnClose()
    {
        transform.DOLocalMove(new Vector3(-1237,0,0),0.5f).SetUpdate(true).OnComplete(()=> { gameObject.SetActive(false); GameManager.Instance.start_time(); });
    }
    public void Onterms()
    {
        Application.OpenURL(Setting_API.Instance.terms_and_conditions);
    }
    public void OnPrivacy()
    {
        Application.OpenURL(Setting_API.Instance.privacy_policy);
    }
    //public void SendEmail()
    //{
    //    string email = "sahajanandinfotech1@gmail.com";
    //    string subject = MyEscapeURL("");
    //    string body = MyEscapeURL("");
    //    Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
    //}
    string MyEscapeURL(string url)
    {
        return WWW.EscapeURL(url).Replace("+", "%20");
    }

    public void toogle_sound()
    {
        //Debug.Log(sound.isOn);
        AudioManager.Instance.Sound_bool = sound.isOn;
        if (sound.isOn)
        {
            sound.GetComponent<Image>().sprite = sound_on;
        }
        else
        {
            sound.GetComponent<Image>().sprite = sound_off;
        }
    }
    public void toogle_music()
    {
        //Debug.Log(music.isOn);
        AudioManager.Instance.Music_bool = music.isOn;
        if (music.isOn)
        {
            music.GetComponent<Image>().sprite = music_on;
        }
        else
        {
            music.GetComponent<Image>().sprite = music_off;
        }
    }
    public void toogle_vibrate()
    {
        //Debug.Log(vibrate.isOn);
        AudioManager.Instance.Vibrate_bool = vibrate.isOn;
        if (vibrate.isOn)
        {
            vibrate.GetComponent<Image>().sprite = vibrate_on;
        }
        else
        {
            vibrate.GetComponent<Image>().sprite = vibrate_off;
        }
    }

}
