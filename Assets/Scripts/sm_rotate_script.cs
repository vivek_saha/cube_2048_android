using UnityEngine;

public class sm_rotate_script : MonoBehaviour
{


    float rotationSpeedX = 90;
    float rotationSpeedY = 0;
    float rotationSpeedZ = 0;
    private Vector3 rotationVector;

    public float speed=1;

    void Update()
    {

        transform.Rotate(new Vector3(rotationSpeedX, rotationSpeedY, rotationSpeedZ) * Time.deltaTime*speed);

    }
    // Start is called before the first frame update
    void Start()
    {
        rotationVector = new Vector3(rotationSpeedX, rotationSpeedY, rotationSpeedZ);
    }

}
