﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public enum withdraw_type { Diamond , Goldcoin}
public class withdraw_but_script : MonoBehaviour
{

    public withdraw_type wth;   

    public Text Diamond_value, Robux_value, cost_value;

    public Image diamond_image, game_diamond;
    

    public string product_id;

    public int coin_cost;

    public Button redeem_but;

    public Sprite gold_coin, diamond;

    public Image fill_image;

    public string skinImage_link;
   
    //public int 


    void Start()
    {
        switch (wth)
        {
            case withdraw_type.Diamond:
                diamond_image.sprite = diamond;
                break;
            case withdraw_type.Goldcoin:
                diamond_image.sprite = gold_coin; 
                break;
            default:
                break;
        }
        //Download_image();
    }


    public void Download_image()
    {
        StartCoroutine(GetTexture());
    }

    IEnumerator GetTexture()
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(Firebase_custome_script.Instance.server_link + skinImage_link);
        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log(www.error);
        }
        else
        {
            Texture2D myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
            game_diamond.sprite = Sprite.Create(myTexture, new Rect(0.0f, 0.0f, myTexture.width, myTexture.height), new Vector2(0.5f, 0.5f), 100.0f);
        }
    }

    public void button_Click()
    {
        redeem_but.onClick.AddListener(()=> open_withdraw_redeem_panel());
    }

    public void set_fragment_fill()
    {

        if(wth== withdraw_type.Goldcoin)
        {
            cost_value.text = Coin_Manager.instance.GoldCoin + "/" +coin_cost.ToString();
            fill_image.fillAmount = (Coin_Manager.instance.GoldCoin / coin_cost) > 1 ? 1 : (Coin_Manager.instance.GoldCoin / coin_cost);
        }
        else if(wth == withdraw_type.Diamond)
        {
            cost_value.text = Coin_Manager.instance.Diamond + "/" + coin_cost.ToString();
            fill_image.fillAmount = (Coin_Manager.instance.Diamond / coin_cost) > 1 ? 1 : (Coin_Manager.instance.Diamond / coin_cost);
        }
        //cost_value.text = PlayerPrefs.GetInt(product_id) + " / " + coin_cost;
        //fill_image.fillAmount = (float)PlayerPrefs.GetInt(product_id) / (float)coin_cost;
    }

    private void open_withdraw_redeem_panel()
    {
        switch (wth)
        {
            case withdraw_type.Diamond:
                if (coin_cost <= Coin_Manager.instance.Diamond)
                {
                    //withdraw_panel
                    Canvas_singleton_script.Instance.Popup_withdraw_panel.GetComponent<Popup_withdraw_panel_script>().p_id = product_id;
                    Canvas_singleton_script.Instance.Popup_withdraw_panel.GetComponent<Popup_withdraw_panel_script>().wth = withdraw_type.Diamond;
                    Canvas_singleton_script.Instance.Popup_withdraw_panel.GetComponent<Popup_withdraw_panel_script>().cost= coin_cost;
                    Canvas_singleton_script.Instance.Popup_withdraw_panel.SetActive(true);
                }
                else
                {
                    //Canvas_singleton_script.Instance.FrageMent_skin_open(Skin_Name.text, SkinImage.sprite);
                    GameManager.Instance.nem_popup_fun("Not Enough Diamonds!");

                }
                break;
            case withdraw_type.Goldcoin:
                if (coin_cost <= Coin_Manager.instance.GoldCoin)
                {
                    //withdraw_panel
                    Canvas_singleton_script.Instance.Popup_withdraw_panel.GetComponent<Popup_withdraw_panel_script>().p_id = product_id;
                    Canvas_singleton_script.Instance.Popup_withdraw_panel.GetComponent<Popup_withdraw_panel_script>().wth = withdraw_type.Goldcoin;
                    Canvas_singleton_script.Instance.Popup_withdraw_panel.GetComponent<Popup_withdraw_panel_script>().cost = coin_cost;
                    Canvas_singleton_script.Instance.Popup_withdraw_panel.SetActive(true);
                }
                else
                {
                    //Canvas_singleton_script.Instance.FrageMent_skin_open(Skin_Name.text, SkinImage.sprite);
                    GameManager.Instance.nem_popup_fun("Not Enough Gold Coins!");

                }
                break;
            default:
                break;
        }

        //throw new NotImplementedException();
    }

    // Update is called once per frame
    void Update()
    {
        set_fragment_fill();

    }
}
