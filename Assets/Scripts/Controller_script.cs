using DG.Tweening;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class Controller_script : MonoBehaviour
{

    public static Controller_script instance;
    private float? lastMousePoint = null;
    public float speed;
    public GameObject prefab_cube;

    public float MAX_POINT, MIN_POINT;
    public float MAX_POINT_diff, MIN_POINT_diff;
    public Canvas coin_canvas;

    public delegate void Newcube(int num);
    public static event Newcube newcubes_;

    public Transform origin_cube;

    GameObject current_cube;

    public int adjuster;

    public List<GameObject> List_of_cubes = new List<GameObject>();


    public GameObject tower_of_cube;




    private void Awake()
    {
        instance = this;
    }



    private void OnEnable()
    {
        cube_script.Collide_hit += Generate_new_cube;
        cube_script.Merge_hit += Merge_hit_fun;
    }

    private void OnDisable()
    {
        cube_script.Collide_hit -= Generate_new_cube;
        cube_script.Merge_hit -= Merge_hit_fun;
    }



    // Start is called before the first frame update
    void Start()
    {


        if (PlayerPrefs.GetInt("First_time", 0) == 0)
        {
            Instantiate(tower_of_cube, Vector3.zero, Quaternion.identity);
            PlayerPrefs.SetInt("First_time", 1);
        }
        Generate_new_cube();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                //Debug.Log("Clicked on the UI");
                AudioManager.Instance.Button_click_sound();
            }
            else
            {

                if (GameManager.Instance.game_start && Time.timeScale == 1)
                {
                    Vector3 mousePos = Input.mousePosition;
                    mousePos.z = Camera.main.nearClipPlane;
                    Vector3 aps = Camera.main.ScreenToWorldPoint(mousePos);

                    lastMousePoint = aps.x;
                    //if(current_cube==null)
                    //{
                        //Generate_new_cube();
                    //}
                    current_cube.transform.GetChild(0).gameObject.SetActive(true);
                    //MIN_POINT = -1.52f;
                    //MAX_POINT = 1.52f;
                }
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            if (lastMousePoint != null && current_cube != null && Time.timeScale == 1)
            {
                lastMousePoint = null;
                current_cube.GetComponent<Rigidbody>().velocity = Vector3.forward * speed;
                //List_of_cubes.Add(current_cube);
                current_cube.transform.GetComponent<cube_script>().hit_bool = false;
                current_cube.transform.GetChild(0).gameObject.SetActive(false);
                current_cube = null;
            }
        }
        if (lastMousePoint != null && Time.timeScale==1)
        {
            if (current_cube != null)
            {
                float difference;
                //= Input.mousePosition.x - lastMousePoint.Value;
                //current_cube.transform.position = Vector3.Lerp(current_cube.transform.position, new Vector3(Mathf.Clamp(current_cube.transform.position.x + (difference / 0.0001f) * Time.deltaTime, -1.52f, 1.52f), current_cube.transform.position.y, current_cube.transform.position.z), Time.deltaTime * 2);
                Vector3 mousePos = Input.mousePosition;
                mousePos.z = Camera.main.nearClipPlane;
                Vector3 aps = Camera.main.ScreenToWorldPoint(mousePos);

                //aps.x= aps.x + difference/100f;
                difference = (aps.x - lastMousePoint.Value) * adjuster;
                //Debug.Log(difference);
                if (difference > 1.52f && difference < MAX_POINT_diff)
                {
                    lastMousePoint = MAX_POINT - (1.52f / adjuster);
                    difference = (aps.x - lastMousePoint.Value) * adjuster;
                    
                }
                else if (difference < -1.52f && difference > MIN_POINT_diff)
                {
                    lastMousePoint = MIN_POINT + (1.52f / adjuster);
                    difference = (aps.x + lastMousePoint.Value) * adjuster;
                }
                else if(difference >= -1.52f && difference <= 1.52f)
                {
                    MAX_POINT_diff = 1.52f;
                    MIN_POINT_diff= -1.52f;
                }
                current_cube.transform.position = new Vector3(Mathf.Clamp((difference), -1.52f, 1.52f), current_cube.transform.position.y, current_cube.transform.position.z);
                if (difference > 1.52f)
                {
                    MAX_POINT_diff = difference;
                    MAX_POINT = aps.x;
                }
                else if (difference < -1.52f)
                {
                    MIN_POINT_diff = difference;
                    MIN_POINT = aps.x;
                }
                //if(difference==1.52f)
                //{
                //    Debug.Log("max_poooint: " + aps.x);
                //}

                //lastMousePoint = Input.mousePosition.x;
            }
        }
    }

    public void Generate_new_cube()
    {
        current_cube = Instantiate(prefab_cube);
        current_cube.transform.localScale = Vector3.zero;
        current_cube.transform.position = origin_cube.position;
        current_cube.transform.rotation = Quaternion.Euler(0, 360f, 0);
        current_cube.transform.GetComponent<cube_script>().hit_bool = true;

        int max = (int)Mathf.Log(PlayerPrefs.GetInt("high_cube", 8), 2);
        if (max > 11)
        {
            max = 6;
        }
        else if (max > 8)
        {
            max = 5;
        }
        else
        {
            max = 3;
        }

        int num = Random.Range(0, max);
        current_cube.GetComponent<MeshRenderer>().material.mainTexture = current_cube.GetComponent<cube_script>().color_list[num];
        current_cube.GetComponent<cube_script>().cube_typpe = Mathf.Pow(2, num + 1).ToString();
        current_cube.transform.DOScale(Vector3.one, 0.5f);
        current_cube.transform.DOLocalRotateQuaternion(Quaternion.Euler(0, 90, 0), 0.5f);
    }

    public void Merge_hit_fun(GameObject a, GameObject b)
    {

        Vector3 pos = (a.transform.position + b.transform.position) / 2;
        GameObject particle = Instantiate(GameManager.Instance.Merge_particle);
        particle.transform.position = pos;
        particle.transform.rotation = Quaternion.identity;
        particle.transform.localScale = Vector3.one;

        AudioManager.Instance.Merge_cube_play();

        Vibration.VibratePop();

        Vector3 position = Camera.main.WorldToScreenPoint(pos);
        position.z = (coin_canvas.transform.position - Camera.main.transform.position).magnitude;
        Vector3 az = Camera.main.ScreenToWorldPoint(position);
        //Debug.Log("screenPointUnscaled : " + az);

        Coin_Manager.instance.Collect_Gold_coin(az);
        Coin_Manager.instance.AddGoldCoin(10);


        //Debug.Log("a = " + a.GetComponent<cube_script>().cube_typpe);
        //Debug.Log("b = " + b.GetComponent<cube_script>().cube_typpe);
        int x = (int)Mathf.Log(int.Parse(a.transform.GetComponent<cube_script>().cube_typpe), 2);
        a.GetComponent<BoxCollider>().enabled = false;
        b.GetComponent<BoxCollider>().enabled = false;
        List_of_cubes.Remove(a);
        List_of_cubes.Remove(b);
        Destroy(a);
        Destroy(b);
        GameObject temp = Instantiate(prefab_cube);
        temp.transform.localScale = Vector3.one;
        temp.transform.position = pos;
        temp.GetComponent<MeshRenderer>().material.mainTexture = temp.GetComponent<cube_script>().color_list[x];
        temp.GetComponent<cube_script>().cube_typpe = Mathf.Pow(2, x + 1).ToString();
        temp.GetComponent<cube_script>().init_cube = false;
        temp.GetComponent<cube_script>().hit_bool = true;
        //temp.GetComponent<cube_script>().merge = true;
        if(Mathf.Pow(2, x + 1) >=128)
        {
           
            //fragment_drop
            float xx = Random.Range(-1.7f, 1.7f);
            float yy = 0.943f;
            float zz = Random.Range(2.54f, 6.14f);
            float sizze = 4.742828f;
            GameObject fraag = Instantiate(GameManager.Instance.fragment_bubble);
            fraag.transform.localScale = new Vector3(sizze,sizze,sizze);
            fraag.transform.position = pos;
            fraag.GetComponent<Fragment_bubble_script>().target = new Vector3(xx, yy, zz);
        }

        //foreach(GameObject and in List_of_cubes)
        //{
        //    if(and==null)
        //    {
        //        List_of_cubes.Remove(and);
        //    }
        //}
        List_of_cubes = List_of_cubes.Where(item => item != null).ToList();


        //explode
        GameObject nm = List_of_cubes.Find(s => s.gameObject.GetComponent<cube_script>().cube_typpe == temp.GetComponent<cube_script>().cube_typpe);
        if (nm != null)
        {
            temp.GetComponent<cube_script>().target = nm;
        }
        else
        {
            if (List_of_cubes.Count > 1)
            {
                temp.GetComponent<cube_script>().target = List_of_cubes[Random.Range(0, List_of_cubes.Count)];
            }
            else
            {
                temp.GetComponent<cube_script>().target = temp;
            }
        }
        temp.GetComponent<cube_script>().explode = true;

        //Debug.Log("temp" + temp.GetComponent<cube_script>().target);

        List_of_cubes.Add(temp);
        temp = null;

        if (Mathf.Pow(2, x + 1) > PlayerPrefs.GetInt("Max_num", 8))
        {
            PlayerPrefs.SetInt("Max_num", (int)Mathf.Pow(2, x + 1));
            GameManager.Instance.stop_time();
            newcubes_(x + 1);
        }
    }

    public void Load_old_cubes(List<PlayerData> datas)
    {
        foreach (PlayerData data in datas)
        {
            GameObject temp = Instantiate(prefab_cube);
            temp.transform.localScale = Vector3.one;
            temp.GetComponent<cube_script>().tower_cube = true;
            temp.transform.localPosition = new Vector3(data._position[0], data._position[1], data._position[2]);
            temp.transform.localRotation = new Quaternion(data._rotation[0], data._rotation[1], data._rotation[2], data._rotation[3]);
            temp.GetComponent<cube_script>().cube_typpe = data.cube_type;
            temp.GetComponent<cube_script>().init_cube = false;
            temp.GetComponent<cube_script>().hit_bool = true;
            temp.GetComponent<cube_script>().merge = true;
        }
    }

    public void Failed_continue_event()
    {
        //remove cube which z is lt 0.5f
        List<GameObject> a = new List<GameObject>();
        a = List_of_cubes.Where(x => x.transform.position.z < 0.5f).ToList();
        //foreach (GameObject z in List_of_cubes)
        //{
        //    if (z.transform.position.z < 0.5f)
        //    {
        //        a.Add(z);
        //    }
        //}

        foreach (GameObject z in a)
        {
            if (List_of_cubes.Contains(z))
            {
                List_of_cubes.Remove(z);
                Destroy(z);
            }
        }
        a = null;
        Canvas_singleton_script.Instance.failed_gameover_panel.SetActive(false);
        GameManager.Instance.start_time();
    }

    public void REstart_game()
    {
        //GameManager.Instance.start_time();
        List<GameObject> a = new List<GameObject>();

        foreach (GameObject f in List_of_cubes)
        {
            a.Add(f);
        }
        //a = List_of_cubes;
        foreach (GameObject z in a)
        {
            if (List_of_cubes.Contains(z))
            {
                List_of_cubes.Remove(z);
                Destroy(z);
            }
        }
        a = null;
        if (current_cube != null)
        {
            Destroy(current_cube);
        }
        Instantiate(tower_of_cube, Vector3.zero, Quaternion.identity);
        PlayerPrefs.SetInt("high_cube", 8);
        Canvas_singleton_script.Instance.failed_gameover_panel.SetActive(false);
        GameManager.Instance.start_time();
        Generate_new_cube();
    }
}
