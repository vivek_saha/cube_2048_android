using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fragment_bubble_script : MonoBehaviour
{

    public Vector3 target;
    // Start is called before the first frame update
    void Start()
    {
        Vector3 mk = calcBallisticVelocityVector(gameObject.transform.position, target, 80);
        //if(mk == nan)
        if (!float.IsNaN(mk.x) && !float.IsNaN(mk.y) && !float.IsNaN(mk.z))
        {
            //Do stuff
            GetComponent<Rigidbody>().AddForce(mk, ForceMode.Impulse);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    Vector3 calcBallisticVelocityVector(Vector3 source, Vector3 target, float angle)
    {
        Vector3 direction = target - source;
        float h = direction.y;
        direction.y = 0;
        float distance = direction.magnitude;
        float a = angle * Mathf.Deg2Rad;
        direction.y = distance * Mathf.Tan(a);
        distance += h / Mathf.Tan(a);

        // calculate velocity
        float velocity = Mathf.Sqrt(distance * Physics.gravity.magnitude / Mathf.Sin(2 * a));
        return velocity * direction.normalized;
    }
    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.gameObject.name != "path" && !collision.gameObject.GetComponent<cube_script>().hit_bool)
    //    {
    //        //this.hit_bool = true;
    //        //this.init_cube = false;
    //        Controller_script.instance.List_of_cubes.Add(this.gameObject);
    //        //Collide_hit();
    //    }
    //}


}
